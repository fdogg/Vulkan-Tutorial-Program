# Vulkan-Tutorial-Program

Small program created with Vulkan Tutorial. Uses some helpful Vulkan libraries, and works with CMake.

If you are having any trouble with your own Vulkan project, feel free to see what's going on here.

## Directories
- src/main/ : C++ Source code
- src/shader/ : GLSL Source code
- lib/ : External library code
- cmake/ : CMake scripts
- build/ : Compiled artifact directory
- .vscode/ : Editor stuff

## Style
Code is indented using tabs. This is meant to save space, and to allow flexibility for whoever is viewing the code to configure their editor's tab size with the preferred spaces equivalent.

## Building
This program builds using CMake. On Linux, CMake can be installed using your friendly neighborhood package manager, but on Windows, I would recommend using MSYS2 UCRT64 for building, which provides a linux-like development environment on windows machines.
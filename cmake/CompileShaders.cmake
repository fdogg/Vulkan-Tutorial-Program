file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/shaders")
foreach(ITEM IN LISTS SHADER_FILES)
	# SHADER_NAME includes file extension: e.g. main.vert
	cmake_path(GET ITEM FILENAME SHADER_NAME)

	string(CONCAT OUT_FILE "${CMAKE_CURRENT_BINARY_DIR}/shaders/${SHADER_NAME}.spv")
	string(REPLACE "${CMAKE_CURRENT_BINARY_DIR}/" "" TRIMMED_OUTFILE ${OUT_FILE})

	add_custom_command(
		COMMAND glslang ARGS -V --quiet -o ${OUT_FILE} ${ITEM}
		MAIN_DEPENDENCY ${ITEM}
		DEPENDS ${ITEM}
		OUTPUT ${OUT_FILE}
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		COMMENT "Building SPIR-V Object ${TRIMMED_OUTFILE}"
		VERBATIM
	)

	target_sources(VulkanTutorial PUBLIC ${OUT_FILE})
endforeach()

#!/bin/bash

# Make the build directory exist
mkdir build

# BEGIN THE CMAKE WONDER !
cmake -G "Ninja" -B build -DCMAKE_EXPORT_COMPILE_COMMANDS=1 --config Debug
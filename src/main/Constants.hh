#pragma once

#include <vulkan/vulkan.h>

#include <string_view>
#include <array>

/** @brief Default window width */
constexpr uint32_t kWindowWidth = 800;
/** @brief Default window height */
constexpr uint32_t kWindowHeight = 600;

/** @brief Number of frames to render concurrently */
constexpr uint32_t kMaxFramesInFlight = 2;

const std::string_view kModelPath = "model.obj";
const std::string_view kModelTexturePath = "model.png";

/** @brief Names of required validation layers */
const std::array<const char*, 1> kValidationLayers = {
	"VK_LAYER_KHRONOS_validation"
};

/** @brief Names of required device extensions */
const std::array<const char*, 1> kDeviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

/**
 * @brief Enables validation layers for the program
 * 
 * Usually this is enabled during debug mode
 */
constexpr bool kEnableValidationLayers =
#if defined(NDEBUG)
	false;
#else
	true;
#endif

constexpr bool kDebug =
#if defined(NDEBUG)
	false;
#else
	true;
#endif

#pragma once

#include "Constants.hh"
#include "utils/Camera.hh"
#include "types/Types.hh"

#include <GLFW/glfw3.h>
#include <vulkan/vulkan.hpp>

#include "vk_mem_alloc.h"

#include <optional>
#include <vector>

/**
 * @brief The application container
 * 
 * This is the class that contains all the variables and methods related
 * to the application
 */
class HelloTriangleApplication {
private:
	/** @brief App window */
	GLFWwindow *mWindow;

	/** @brief Vulkan instance */
	vk::Instance mInstance;

	/** @brief Debug messenger */
	vk::DebugUtilsMessengerEXT mDebugMessenger;

	vk::SurfaceKHR mSurface;

	/** @brief Physical device*/
	vk::PhysicalDevice mPhysicalDevice = nullptr;
	/** @brief Device */
	vk::Device mDevice;

	// Queues are created with the device
	vk::Queue mGraphicsQueue;
	vk::Queue mPresentQueue;

	VmaAllocator mAllocator;

	struct Image {
		vk::Image image;
		vk::ImageView view;
	};

	vk::SwapchainKHR mSwapChain;
	std::vector<Image> mSwapChainImages;
	vk::Format mSwapChainImageFormat;
	vk::Extent2D mSwapChainExtent;

	vk::RenderPass mRenderPass;

	vk::DescriptorSetLayout mDescriptorSetLayout;

	VkPipelineLayout mPipelineLayout;

	vk::Pipeline mGraphicsPipeline;

	std::vector<vk::Framebuffer> mSwapChainFramebuffers;

	/** @brief Command pool */
	vk::CommandPool mCommandPool;

	uint32_t mCurrentFrame = 0;

	bool mFramebufferResized = false;

	/* Vertex & Index stuff */
	std::vector<Vertex> mVertices;
	std::vector<uint32_t> mIndices;
	vk::Buffer mVertexBuffer;
	VmaAllocation mVertBufferAllocation;
	vk::Buffer mIndexBuffer;
	VmaAllocation mIndexBufferAllocation;
	/* End vertex & Index stuff*/

	vk::DescriptorPool mDescriptorPool;

	/* ImGui Stuff */
	vk::DescriptorPool mImguiDescriptorPool;
	/* End ImGui Stuff */

	/**
	 * @brief Enacpsulation for data required to render a frame
	 */
	struct FrameData {
		/** @brief The command buffer for the frame */
		vk::CommandBuffer commandBuffer;

		vk::Semaphore imgAvailableSemaphore;
		vk::Semaphore renderFinishedSemaphore;
		vk::Fence inFlightFence;

		/** @brief Frame uniform buffer */
		vk::Buffer uniformBuffer;
		/** @brief Memory for the uniform buffer */
		VmaAllocation uniformBufferAllocation;
		/** @brief Pointer to the buffer data in RAM */
		void* uniformBufferMapped;

		/** @brief Descriptor set */
		vk::DescriptorSet descriptorSet;
	};

	/** @brief Frame data */
	std::array<FrameData, kMaxFramesInFlight> mFrames;

	vk::Image mTextureImage;
	vk::DeviceMemory mTextureImageMemory;
	vk::ImageView mTextureImageView;
	vk::Sampler mTextureImageSampler;

	vk::Image mDepthImage;
	vk::DeviceMemory mDepthImageMemory;
	vk::ImageView mDepthImageView;

	float mModelRotationDegs = 0.0f;
	float mModelZoom = 1.0f;

	/** @brief First person camera for app */
	utils::Camera mCam;

	/** @brief ImGui is usable */
	bool mMouseEnabled = true;

	struct {
		glm::vec3 matSpecular { 0.3f, 0.3f, 0.3f };
		glm::vec3 matDiffuse { 0.3f, 0.3f, 0.3f };
		float ambient { 0.1f };
		float shininess { 5.0f };
		glm::vec3 lightSpecular { 0.1f, 0.1f, 0.1f };
		glm::vec3 lightDiffuse { 0.1f, 0.1f, 0.1f };
		glm::vec3 lightPosition { 0.0f, 0.0f, 0.8f };
	} mShadingData;

private:
	inline FrameData& GetCurrentFrameData() { return this->mFrames[this->mCurrentFrame]; };

public:
	/**
	 * @brief Runs the application
	 */
	void Run();

private:
	/**
	 * @brief Initialize the window
	 * 
	 * Sets up GLFW and all of its related windowing needs
	 */
	void InitWindow();

	/**
	 * @brief Initialize Vulkan
	 * 
	 * Sets up all the Vulkan components required to make the app run
	 */
	void InitVulkan();

	/**
	 * @brief Create the application instance
	 */
	void CreateInstance();

	/**
	 * @brief Method that is called to run the frame
	 * 
	 * This method initiates running the main loop. The function does not exit
	 * until the window is ready to close.
	 */
	void MainLoop();

	/**
	 * @brief Cleans up the application
	 * 
	 * Destroys all the created objects
	 */
	void Cleanup();

	/**
	 * @brief Checks to see if Validation Layers are supported
	 * 
	 * @return true Validation layers are supported
	 * @return false Validation layers are not supported
	 */
	bool CheckValidationLayerSupport() const;

	/**
	 * @brief Get the extensions required to run
	 * 
	 * Gets the Vulkan extensions required by GLFW to run properly and also
	 * any other additional extensions that we may want
	 * @return std::vector<const char*> The required extensions
	 */
	std::vector<const char*> GetRequiredExtensions() const;

	/**
	 * @brief Callback for the validation layers
	 * 
	 * This is the function that is called when the validation layers are run
	 */
	static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
		void *pUserData
	);

	/**
	 * @brief Sets up the Debug Messenger
	 * 
	 * If validation layers are disabled, everything in this function will be skipped
	 */
	void SetupDebugMessenger();

	/**
	 * @brief Fills out the properties of a VkDebugUtilsMessengerCreateInfoEXT struct
	 * 
	 * @param createInfo The struct to fill out
	 */
	static void PopulateDebugMessengerCreateInfo(vk::DebugUtilsMessengerCreateInfoEXT& createInfo);

	/**
	 * @brief Finds a good device to run Vulkan on
	 * 
	 * Goes through all possible GPUs and picks the best device to
	 * render on
	 */
	void PickPhysicalDevice();

	/**
	 * @brief Checks if a device is good to run Vulkan on
	 * 
	 * Checks if the device supports the required extensions and is
	 * able to support the swap chain as well
	 * @param device The device to check
	 * @return true The device is suitable
	 * @return false The device is not suitable
	 */
	bool IsDeviceSuitable(vk::PhysicalDevice device);

	/**
	 * @brief Checks to see if the device supports the required extensions
	 * @param device The device to check
	 * @return true The device supports the required extensions
	 * @return false The device does not support the required extensions
	 */
	bool CheckDeviceExtensionSupport(vk::PhysicalDevice device);

	/**
	 * @brief A struct that holds indices for the queue families that we would want
	 */
	struct QueueFamilyIndices {
		/** @brief The index of the queue family that supports graphics commands */
		std::optional<uint32_t> graphicsFamily;
		/** @brief The index of the queue family that supports present commands */
		std::optional<uint32_t> presentFamily;

		/**
		 * @brief Checks if the struct is completely filled out
		 * @return bool Weather the struct is complete or not
		 */
		bool IsComplete() const noexcept;
	};

	/**
	 * @brief Looks for all of the queue families that we might need
	 *
	 * Queue families are groups of queues that can each do a different thing (Graphics, Compute, Transfer, ...)
	 * 
	 * @param device The device to query
	 * @return QueueFamilyIndices The found queue families
	 */
	[[nodiscard]]
	QueueFamilyIndices FindQueueFamilies(vk::PhysicalDevice device);

	/**
	 * @brief Creates the Logical Device
	 */
	void CreateLogicalDevice();

	/** 
	 * @brief Creates the window surface
	 */
	void CreateSurface();

	/**
	 * @brief Information about the swap chain and
	 * what it supports
	 */
	struct SwapChainSupportDetails {
		vk::SurfaceCapabilitiesKHR capabilities;
		std::vector<vk::SurfaceFormatKHR> formats;
		std::vector<vk::PresentModeKHR> presentModes;
	};

	/**
	 * @brief Gets the swap chain support for the selected device
	 * @param device The device to query
	 * @return SwapChainSupportDetails 
	 */
	[[nodiscard]]
	SwapChainSupportDetails QuerySwapChainSupport(vk::PhysicalDevice device);

	/**
	 * @brief Gets the best surface format available for the program
	 * @param availableFormats The formats to choose from
	 * @return VkSurfaceFormatKHR The best surface format to use for the program
	 */
	[[nodiscard]]
	vk::SurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats);

	/**
	 * @brief Pick a present mode out of a list of supported present modes
	 * 
	 * @param availablePresentModes The supported present modes
	 * @return vk::PresentModeKHR A present mode best suited to what we wnat to use
	 */
	[[nodiscard]]
	vk::PresentModeKHR ChooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availablePresentModes);

	/**
	 * @brief Sets the swapchain size
	 * 
	 * @param capabilities The capabilities of the swapchain
	 * @return vk::Extent2D 
	 */
	[[nodiscard]]
	vk::Extent2D ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilities);

	/**
	 * @brief Create the swap chain
	 */
	void CreateSwapChain();

	/**
	 * @brief Creates the image views
	 * 
	 * Image views are important because they describe how our program will 'view' or interact with the swapchain's
	 * images. This function goes through all of the swapchain's images and creates image views for them.
	 *
	 * Fills out the array of image views with new image views objects
	 */
	void CreateImageViews();

	/**
	 * @brief Creates the graphics pipeline
	 */
	void CreateGraphicsPipeline();

	/**
	 * @brief Creates a shader module
	 * 
	 * Creates a Vulkan shader from the provided compiled shader code
	 * @param code SpirV code to create the shader from
	 * @return VkShaderModule The created Vulkan shader object
	 */
	[[nodiscard]]
	vk::ShaderModule CreateShaderModule(const std::vector<char>& code);

	/**
	 * @brief Creates the render pass
	 */
	void CreateRenderPass();

	/**
	 * @brief Creates the framebuffers
	 * 
	 * Creates all the necessary framebuffers for the swap chain
	 */
	void CreateFramebuffers();

	/**
	 * @brief Creates the command pool
	 */
	void CreateCommandPool();

	/**
	 * @brief Creates the command buffers
	 * 
	 * Makes a command buffer for each framebuffer that will be rendered in
	 * flight
	 */
	void CreateCommandBuffers();

	/**
	 * @brief Creates the synchronization objects
	 * 
	 * Creates all the semaphores and fences required to sync the GPU operations
	 * with the CPU
	 */
	void CreateSyncObjects();

	/**
	 * @brief Populates the command buffer
	 * 
	 * Fills the command buffer with Vulkan commands to send to the GPU
	 * @param commandBuffer Command buffer to record to
	 * @param imageIndex Index of the image currently being rendered
	 */
	void RecordCommandBuffer(vk::CommandBuffer commandBuffer, uint32_t imageIndex);

	/**
	 * @brief Draws a frame
	 * 
	 * Sends Vulkan commands to the GPU to draw a frame on the screen
	 */
	void DrawFrame();

	/**
	 * @brief Recreates the swap chain
	 * 
	 * This method is used to originally create the swap chain and is also
	 * used to recreate it when the window is resized.
	 */
	void RecreateSwapChain();

	/**
	 * @brief Cleans up everything related to the swap chain
	 */
	void CleanupSwapChain();

	/**
	 * @brief Callback for when the window resizes
	 * 
	 * This method causes the swap chain to be recreated for the current
	 * window size
	 */
	static void FramebufferResizeCallback(GLFWwindow *window, int width, int height);

	/**
	 * @brief Callback for key evernts
	 * 
	 * Closes the program if the escape key has been pressed
	 */
	static void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);

	/**
	 * @brief Callback for mouse events
	 *
	 * Updates the first person camera
	 */
	static void MouseCallback(GLFWwindow *window, double xpos, double ypos);

	/**
	 * @brief Creates the vertex buffer
	 * 
	 * This will create a vertex buffer and fill it with the triangle's vertices
	 */
	void CreateVertexBuffer();

	/**
	 * @brief Find a memory type for the current physical device
	 * 
	 * @param typeFilter 
	 * @param properties 
	 * @return uint32_t 
	 */
	[[nodiscard]]
	uint32_t FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties) const;

	/**
	 * @brief Copies a buffer
	 * 
	 * @param srcBuffer Buffer to copy from
	 * @param dstBuffer Buffer to copy into
	 * @param size Size of both buffers
	 */
	void CopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);

	/**
	 * @brief Creates the Index Buffer
	 */
	void CreateIndexBuffer();

	/**
	 * @brief Creates the Descriptor Set layout
	 */
	void CreateDescriptorSetLayout();

	/**
	 * @brief Create the Uniform Buffers
	 */
	void CreateUniformBuffers();

	/**
	 * @brief Updates the uniform buffer
	 * @param currentImage The current image that is being rendered
	 */
	void UpdateUniformBuffer(uint32_t currentImage);

	/**
	 * @brief Creates the Descriptor Pool
	 */
	void CreateDescriptorPool();

	/**
	 * @brief Create the Descriptor Sets
	 */
	void CreateDescriptorSets();

	/**
	 * @brief Creates the texture image
	 */
	void CreateTextureImage();

	/**
	 * @brief Create a Vulkan Image and corresponding Device Memory
	 * 
	 * @param width Image width
	 * @param height Image height
	 * @param format Image format
	 * @param tiling Image tiling
	 * @param usage Image usage
	 * @param properties Memory properties
	 * @param image The image
	 * @param imageMemory The image emeory
	 */
	void CreateImage(
		uint32_t width, uint32_t height,
		vk::Format format, vk::ImageTiling tiling,
		vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties,
		vk::Image& image, vk::DeviceMemory& imageMemory
	);

	/**
	 * @brief Creates and begins a temporary command buffer
	 * 
	 * @return vk::CommandBuffer The temporary command buffer
	 */
	[[nodiscard]]
	vk::CommandBuffer BeginSingleTimeCommands();

	/**
	 * @brief Ends the temporary command buffer
	 * 
	 * Should call device.waitIdle(); after
	 * @param commandBuffer The command buffer to end
	 */
	void EndSingleTimeCommandBuffer(vk::CommandBuffer commandBuffer);

	/**
	 * @brief Transitions the image layout from one to another
	 * 
	 * @param image Image to transition
	 * @param format Image format
	 * @param oldLayout Old (current) image layout
	 * @param newLayout New image layout to transition to
	 */
	void TransitionImageLayout(
		vk::Image image, vk::Format format,
		vk::ImageLayout oldLayout, vk::ImageLayout newLayout
	);

	/**
	 * @brief Copies a buffer into a VkImage
	 * 
	 * @param buffer The buffer to copy
	 * @param image The image to copy into
	 * @param width Width
	 * @param height Height
	 */
	void CopyBufferToImage(vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height);

	/**
	 * @brief Creates the Texture Image View with the right format
	 */
	void CreateTextureImageView();

	/**
	 * @brief Create an Image View
	 * 
	 * @param image The image to create from
	 * @param format Image format
	 * @param aspectFlags Aspect flags
	 * @return vk::ImageView The image view
	 */
	[[nodiscard]]
	vk::ImageView CreateImageView(vk::Image image, vk::Format format, vk::ImageAspectFlagBits aspectFlags);

	/**
	 * @brief Creates the Texture Sampler
	 */
	void CreateTextureSampler();

	/**
	 * @brief Creates the depth buffer & related things
	 */
	void CreateDepthResource();

	/**
	 * @brief Finds format matching certain criteria
	 * 
	 * @param candidates Candidates for selection
	 * @param tiling Tiling to select
	 * @param features Other manditory features
	 * @return vk::Format A format that matches criteria
	 */
	[[nodiscard]]
	vk::Format FindSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features) const;

	/**
	 * @brief Finds a supported depth format to use
	 * 
	 * @return vk::Format The found depth format
	 */
	[[nodiscard]]
	vk::Format FindDepthFormat() const;

	/**
	 * @brief Checks if the format is a stencil component
	 * 
	 * @param format Format to check
	 * @return true The format is a stencil component
	 * @return false The format is not a stencil component
	 */
	[[nodiscard]]
	static bool HasStencilComponent(vk::Format format);

	/**
	 * @brief Initializes ImGui
	 */
	void InitImgui();

	/**
	 * @brief Describes ImGui windows to be rendered
	 */
	void ImguiDrawing();

	/**
	 * @brief Render the ImGui draw data
	 * 
	 * @param cmd Command buffer to push commands to
	 */
	void ImguiRender(vk::CommandBuffer& cmd);

	/**
	 * @brief Loads the model into the application's vertices and indices
	 */
	void LoadModel();

	/**
	 * @brief Checks the keyboard input each frame
	 */
	void PollKeyboardInput();

	/**
	 * @brief Creates the VmaAllocator object used to perform subsequent allocations
	 */
	void CreateAllocator();
};

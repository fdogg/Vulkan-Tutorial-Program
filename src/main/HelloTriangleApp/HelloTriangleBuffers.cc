#include "HelloTriangleApp/HelloTriangleApplication.hh"
#include <stdexcept>
#include <vulkan/vulkan_enums.hpp>

void HelloTriangleApplication::CreateVertexBuffer() {
	// Calculate buffer size
	const vk::DeviceSize bufferSize = sizeof(Vertex) * this->mVertices.size();

	// Staging buffer stuff
	vk::Buffer stagingBuffer;
	VmaAllocation stagingBufferAllocation;

	vk::BufferCreateInfo stagingBufInfo {
		.size = bufferSize,
		.usage = vk::BufferUsageFlagBits::eTransferSrc,
		.sharingMode = vk::SharingMode::eExclusive
	};

	VmaAllocationCreateInfo allocInfo { };
	allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

	if(vmaCreateBuffer(
		this->mAllocator,
		(const VkBufferCreateInfo*)&stagingBufInfo,
		&allocInfo,
		(VkBuffer*)&stagingBuffer,
		&stagingBufferAllocation,
		nullptr
	) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Unable to alloc staging buffer");

	// Write vertex data to staging buffer

	void *data;

	if(vmaMapMemory(this->mAllocator, stagingBufferAllocation, &data) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Failed to map memory!");

	std::memcpy(data, reinterpret_cast<const void*>(this->mVertices.data()), static_cast<size_t>(bufferSize));

	vmaUnmapMemory(this->mAllocator, stagingBufferAllocation);

	// Create vertex buffer

	vk::BufferCreateInfo vertexBuffInfo {
		.size = bufferSize,
		.usage = vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer,
		.sharingMode = vk::SharingMode::eExclusive
	};

	allocInfo = { };
	allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

	if(vmaCreateBuffer(
		this->mAllocator,
		(const VkBufferCreateInfo*)&vertexBuffInfo,
		&allocInfo,
		(VkBuffer*)&this->mVertexBuffer,
		&this->mVertBufferAllocation,
		nullptr
	) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Unable to create vertex buffer");

	this->CopyBuffer(stagingBuffer, this->mVertexBuffer, bufferSize);

	vmaDestroyBuffer(this->mAllocator, stagingBuffer, stagingBufferAllocation);
}

void HelloTriangleApplication::CreateIndexBuffer() {
	// Calculate buffer size
	const vk::DeviceSize bufferSize = sizeof(uint32_t) * this->mIndices.size();

	// Staging buffer stuff
	vk::Buffer stagingBuffer;
	VmaAllocation stagingBufferAllocation;

	vk::BufferCreateInfo stagingBufferInfo {
		.size = bufferSize,
		.usage = vk::BufferUsageFlagBits::eTransferSrc,
		.sharingMode = vk::SharingMode::eExclusive	
	};

	VmaAllocationCreateInfo allocInfo { };
	allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

	if(vmaCreateBuffer(
		this->mAllocator,
		(const VkBufferCreateInfo*)&stagingBufferInfo,
		&allocInfo,
		(VkBuffer*)&stagingBuffer,
		&stagingBufferAllocation,
		nullptr
	) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Unable to alloc staging buffer");

	// Write data to index buffer
	void *data;

	if(vmaMapMemory(this->mAllocator, stagingBufferAllocation, &data) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Failed to map memory!");

	std::memcpy(data, this->mIndices.data(), bufferSize);

	vmaUnmapMemory(this->mAllocator, stagingBufferAllocation);

	// Create index buffer

	vk::BufferCreateInfo indexBuffInfo {
		.size = bufferSize,
		.usage = vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer,
		.sharingMode = vk::SharingMode::eExclusive
	};

	allocInfo = { };
	allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

	if(vmaCreateBuffer(
		this->mAllocator,
		(const VkBufferCreateInfo*)&indexBuffInfo,
		&allocInfo,
		(VkBuffer*)&this->mIndexBuffer,
		&this->mIndexBufferAllocation,
		nullptr
	) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Unable to create vertex buffer");

	this->CopyBuffer(stagingBuffer, this->mIndexBuffer, bufferSize);

	vmaDestroyBuffer(this->mAllocator, stagingBuffer, stagingBufferAllocation);
}

void HelloTriangleApplication::CopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
	vk::CommandBuffer commandBuffer = this->BeginSingleTimeCommands();
	{
		vk::BufferCopy copyRegion {
			.srcOffset = 0,
			.dstOffset = 0,
			.size = size
		};
		commandBuffer.copyBuffer(srcBuffer, dstBuffer, copyRegion);
	}
	this->EndSingleTimeCommandBuffer(commandBuffer);
}

void HelloTriangleApplication::CreateCommandBuffers() {
	vk::CommandBufferAllocateInfo createInfo {
		.commandPool = this->mCommandPool,
		.level = vk::CommandBufferLevel::ePrimary,
		.commandBufferCount = static_cast<uint32_t>(kMaxFramesInFlight)
	};

	std::vector<vk::CommandBuffer> commandBuffers = this->mDevice.allocateCommandBuffers(createInfo);

	for(uint32_t i = 0; i < this->mFrames.size(); i++) {
		this->mFrames[i].commandBuffer = commandBuffers[i];
	}
}

void HelloTriangleApplication::CreateUniformBuffers() {
	// Retrieve size of uniform buffer
	const vk::DeviceSize bufferSize = sizeof(UniformBufferObject);

	// FIll out each uniform buffer per frame
	for(auto& frame : this->mFrames) {
		vk::BufferCreateInfo uniformBufferInfo {
			.size = bufferSize,
			.usage = vk::BufferUsageFlagBits::eUniformBuffer,
			.sharingMode = vk::SharingMode::eExclusive
		};

		VmaAllocationCreateInfo allocInfo { };
		allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

		if(vmaCreateBuffer(
			this->mAllocator,
			(const VkBufferCreateInfo*)&uniformBufferInfo,
			&allocInfo,
			(VkBuffer*)&frame.uniformBuffer,
			&frame.uniformBufferAllocation,
			nullptr
		) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Unable to alloc staging buffer");

		if(vmaMapMemory(this->mAllocator, frame.uniformBufferAllocation, &frame.uniformBufferMapped) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Failed to map memory!");
	}
}

vk::CommandBuffer HelloTriangleApplication::BeginSingleTimeCommands() {
	// Begin one time submit command buffer
	vk::CommandBufferAllocateInfo allocInfo {
		.commandPool = this->mCommandPool,
		.level = vk::CommandBufferLevel::ePrimary,
		.commandBufferCount = 1,
	};

	vk::CommandBuffer commandBuffer;
	commandBuffer = this->mDevice.allocateCommandBuffers(allocInfo)[0];

	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit
	};

	commandBuffer.begin(beginInfo);

	return commandBuffer;
}

void HelloTriangleApplication::EndSingleTimeCommandBuffer(vk::CommandBuffer commandBuffer) {
	// End one time submit command buffer
	commandBuffer.end();

	vk::SubmitInfo submitInfo {
		.commandBufferCount = 1,
		.pCommandBuffers = &commandBuffer
	};

	this->mGraphicsQueue.submit(submitInfo, nullptr);
	this->mGraphicsQueue.waitIdle();

	this->mDevice.freeCommandBuffers(this->mCommandPool, commandBuffer);
}

void HelloTriangleApplication::CopyBufferToImage(vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height) {
	// Copies the whole buffer as a region to an image
	vk::CommandBuffer commandBuffer = this->BeginSingleTimeCommands();
	{
		vk::BufferImageCopy region {
			.bufferOffset = 0,
			.bufferRowLength = 0,
			.bufferImageHeight = 0,
			.imageSubresource = {
				.aspectMask = vk::ImageAspectFlagBits::eColor,
				.mipLevel = 0,
				.baseArrayLayer = 0,
				.layerCount = 1
			},
			.imageOffset = { 0, 0, 0 },
			.imageExtent = { width, height, 1 }
		};

		commandBuffer.copyBufferToImage(buffer, image, vk::ImageLayout::eTransferDstOptimal, region);
	}
	this->EndSingleTimeCommandBuffer(commandBuffer);
}

void HelloTriangleApplication::CreateAllocator() {
	// Create the VMA allocator
	VmaAllocatorCreateInfo allocInfo { };
	allocInfo.device = this->mDevice;
	allocInfo.physicalDevice = this->mPhysicalDevice;
	allocInfo.instance = this->mInstance;
	allocInfo.vulkanApiVersion = VK_API_VERSION_1_3;
	
	vmaCreateAllocator(&allocInfo, &this->mAllocator);
}

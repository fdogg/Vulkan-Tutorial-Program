#include "HelloTriangleApplication.hh"

#include <cstring>
#include <set>

bool HelloTriangleApplication::CheckDeviceExtensionSupport(vk::PhysicalDevice device) {
	// Get extensions
	std::vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties(nullptr);

	// Fill a set with the required extensions
	std::set<std::string> requiredExtensions(kDeviceExtensions.begin(), kDeviceExtensions.end());

	// Erase the extensions from the set that are contained in the device extension vector
	for(const auto& extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}

	// If all the extensions in the extension vector are in the required set, then
	// the set should be empty by now
	return requiredExtensions.empty();
}

bool HelloTriangleApplication::IsDeviceSuitable(vk::PhysicalDevice device) {
	QueueFamilyIndices indices = this->FindQueueFamilies(device);

	bool extensionsSupported = this->CheckDeviceExtensionSupport(device);

	bool swapChainAdequate = false;
	if(extensionsSupported) {
		SwapChainSupportDetails swapChainSupport = this->QuerySwapChainSupport(device);
		swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	vk::PhysicalDeviceFeatures deviceFeatures = device.getFeatures();

	return indices.IsComplete() && extensionsSupported && swapChainAdequate && deviceFeatures.samplerAnisotropy;
}

bool HelloTriangleApplication::CheckValidationLayerSupport() const {
	// Populate a vector with the available layers
	std::vector<vk::LayerProperties> layers = vk::enumerateInstanceLayerProperties();

	// Check to see if the available layers have the layers we want
	for(const char* layerName : kValidationLayers) {
		bool layerFound = false;

		for(const auto& layer : layers) {
			if(std::strcmp(layerName, layer.layerName) == 0) {
				layerFound = true;
				break;
			}
		}

		if(!layerFound) {
			return false;
		}
	}

	return true;
}

bool HelloTriangleApplication::HasStencilComponent(vk::Format format) {
	return format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint;
}

#include "HelloTriangleApplication.hh"

#include "utils/DebugUtils.hh"
#include "log/loguru.hh"

VKAPI_ATTR VkBool32 VKAPI_CALL HelloTriangleApplication::DebugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
	void *pUserData) {

	(void)messageType;
	(void)pCallbackData;
	(void)pUserData;

	if(messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
		LOG_F(ERROR, "%s", pCallbackData->pMessage);	
	} else if(messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
		LOG_F(8, "%s", pCallbackData->pMessage);
	} else [[likely]] {
		LOG_F(9, "%s", pCallbackData->pMessage);	
	}

	return VK_FALSE;
}

void HelloTriangleApplication::SetupDebugMessenger() {
	if constexpr(!kEnableValidationLayers) return;

	// Populate the create info struct
	vk::DebugUtilsMessengerCreateInfoEXT createInfo;
	this->PopulateDebugMessengerCreateInfo(createInfo);

	if(
		CreateDebugUtilsMessengerEXT(
			this->mInstance, reinterpret_cast<VkDebugUtilsMessengerCreateInfoEXT*>(&createInfo),
			nullptr, reinterpret_cast<VkDebugUtilsMessengerEXT*>(&this->mDebugMessenger)
		) != VK_SUCCESS
	) [[unlikely]] {
		throw std::runtime_error("Failed to setup debug messenger");
	}
}

void HelloTriangleApplication::PopulateDebugMessengerCreateInfo(vk::DebugUtilsMessengerCreateInfoEXT& createInfo) {
	createInfo = {
		.messageSeverity =
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
			vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
		.messageType =
			vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
			vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
			vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
		.pfnUserCallback = HelloTriangleApplication::DebugCallback
	};
}

/*
// Program to allow string literals in templates

#include <algorithm>

template<size_t N>
struct TemplateString {
	constexpr TemplateString(const char (&str)[N]) {
		std::copy_n(str, N, value);
	}

	char value[N];
};

template<TemplateString str>
void PrintStringLiteral() {
	constexpr size_t size = sizeof(str.value);
	constexpr auto contents = str.value;

	std::cout << "Size: " << size << ", contents: " << contents << std::endl;
}

void Something() {
	PrintStringLiteral<"Hello">();
}

*/
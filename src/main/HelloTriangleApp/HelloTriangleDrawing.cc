#include "HelloTriangleApplication.hh"

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/trigonometric.hpp>

#include <stdexcept>
#include <limits>

void HelloTriangleApplication::MainLoop() {
	// Run the main loop
	while(!glfwWindowShouldClose(this->mWindow)) {
		glfwPollEvents();
		this->PollKeyboardInput();
		this->DrawFrame();
	}

	this->mDevice.waitIdle();
}

void HelloTriangleApplication::DrawFrame() {
	// Wait for previous frame to be drawn
	if(this->mDevice.waitForFences(1, &this->GetCurrentFrameData().inFlightFence, true, std::numeric_limits<uint64_t>::max()) != vk::Result::eSuccess) {
		throw std::runtime_error("Unable to wait for fence (previous frame to finish)");
	}

	vk::ResultValue<uint32_t> resultValue = this->mDevice.acquireNextImageKHR(this->mSwapChain, std::numeric_limits<uint64_t>::max(), this->GetCurrentFrameData().imgAvailableSemaphore, nullptr);
	vk::Result result = resultValue.result;

	if(result == vk::Result::eErrorOutOfDateKHR) {
		this->RecreateSwapChain();
		return;
	} else if(result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR) {
		throw std::runtime_error("Failed to acquire swap chain image");
	}

	uint32_t imageIndex = resultValue.value;
	this->UpdateUniformBuffer(this->mCurrentFrame);

	this->mDevice.resetFences(this->GetCurrentFrameData().inFlightFence);

	this->GetCurrentFrameData().commandBuffer.reset({ });

	this->ImguiDrawing();

	this->RecordCommandBuffer(this->GetCurrentFrameData().commandBuffer, imageIndex);

	vk::Semaphore waitSemaphores[] = { this->GetCurrentFrameData().imgAvailableSemaphore };
	vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };

	vk::Semaphore signalSemaphores[] = { this->GetCurrentFrameData().renderFinishedSemaphore };

	vk::SubmitInfo submitInfo {
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = waitSemaphores,
		.pWaitDstStageMask = waitStages,

		.commandBufferCount = 1,
		.pCommandBuffers = &this->GetCurrentFrameData().commandBuffer,

		.signalSemaphoreCount = 1,
		.pSignalSemaphores = signalSemaphores
	};

	this->mGraphicsQueue.submit(submitInfo, this->GetCurrentFrameData().inFlightFence);

	vk::SwapchainKHR swapChains[] = { this->mSwapChain };

	vk::PresentInfoKHR presentInfo {
		.waitSemaphoreCount = 1,
		.pWaitSemaphores = signalSemaphores,
		.swapchainCount = 1,
		.pSwapchains = swapChains,
		.pImageIndices = &imageIndex,
		.pResults = nullptr
	};

	result = this->mPresentQueue.presentKHR(presentInfo);

	switch(result) {
		case vk::Result::eErrorOutOfDateKHR:
		case vk::Result::eSuboptimalKHR:
			this->mFramebufferResized = true;
			this->RecreateSwapChain();
			break;
		case vk::Result::eSuccess: [[likely]] break;
		default: [[unlikely]]
			throw std::runtime_error("Failed to present swapchain image");
	}

	this->mCurrentFrame = (this->mCurrentFrame + 1) % kMaxFramesInFlight;
}


void HelloTriangleApplication::RecordCommandBuffer(vk::CommandBuffer commandBuffer, uint32_t imageIndex) {
	vk::CommandBufferBeginInfo beginInfo {
		.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit
	};

	commandBuffer.begin(beginInfo);
	{
		std::array<vk::ClearValue, 2> clearValues {{
			{ .color = { .float32 = {{ 0.0f, 0.0f, 0.0f, 1.0f }} } },
			{ .depthStencil = { 1.0f, 0 } }
		}};

		vk::RenderPassBeginInfo renderPassInfo {
			.renderPass = this->mRenderPass,
			.framebuffer = this->mSwapChainFramebuffers[imageIndex],
			.renderArea = {
				.offset = { 0, 0 },
				.extent = this->mSwapChainExtent
			},
			.clearValueCount = static_cast<uint32_t>(clearValues.size()),
			.pClearValues = clearValues.data()
		};

		commandBuffer.beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);

		commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, this->mGraphicsPipeline);

		vk::Viewport viewport {
			.x = 0.0f,
			.y = 0.0f,
			.width = static_cast<float>(this->mSwapChainExtent.width),
			.height = static_cast<float>(this->mSwapChainExtent.height),
			.minDepth = 0.0f,
			.maxDepth = 1.0f
		};
		commandBuffer.setViewport(0, viewport);

		vk::Rect2D scissor {
			.offset = { 0, 0 },
			.extent = this->mSwapChainExtent
		};
		commandBuffer.setScissor(0, scissor);

		std::array<vk::Buffer, 1> vertexBuffers = { this->mVertexBuffer };
		std::array<vk::DeviceSize, 1> offsets = { 0 };
		commandBuffer.bindVertexBuffers(0, vertexBuffers, offsets);

		commandBuffer.bindIndexBuffer(this->mIndexBuffer, 0, vk::IndexType::eUint32);

		commandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, this->mPipelineLayout, 0, this->GetCurrentFrameData().descriptorSet, nullptr);

		commandBuffer.drawIndexed(static_cast<uint32_t>(this->mIndices.size()), 1, 0, 0, 0);

		this->ImguiRender(commandBuffer);

		commandBuffer.endRenderPass();
	}
	commandBuffer.end();
}

void HelloTriangleApplication::RecreateSwapChain() {
	int width = 0, height = 0;
	glfwGetFramebufferSize(this->mWindow, &width, &height);
	while(width == 0 || height == 0) {
		glfwGetFramebufferSize(this->mWindow, &width, &height);
		glfwWaitEvents();
	}

	this->mDevice.waitIdle();

	this->CleanupSwapChain();

	this->CreateSwapChain();
	this->CreateImageViews();
	this->CreateDepthResource();
	this->CreateFramebuffers();
}

void HelloTriangleApplication::UpdateUniformBuffer(const uint32_t currentImage) {
	const static glm::mat4 rotMat = glm::rotate(
		glm::identity<glm::mat4>(),
		glm::radians(-90.0f),
		glm::vec3(1.0, 0.0, 0.0)
	);

	UniformBufferObject ubo {
		.model =
			glm::rotate(glm::identity<glm::mat4>(), glm::radians(this->mModelRotationDegs), glm::vec3(0.0f, 0.0f, 1.0f)) *
			glm::scale(glm::identity<glm::mat4>(), glm::vec3(this->mModelZoom)),
		.view = this->mCam.GetCameraMatrix() * rotMat,
		.proj = glm::perspective(
			glm::radians(45.0f),
			static_cast<float>(this->mSwapChainExtent.width) / static_cast<float>(this->mSwapChainExtent.height),
			0.04f, 12.0f
		),
		.mspec = this->mShadingData.matSpecular,
		.mdiff = this->mShadingData.matDiffuse,
		.amb = this->mShadingData.ambient,
		.mshin = this->mShadingData.shininess,
		.lspec = this->mShadingData.lightSpecular,
		.ldiff = this->mShadingData.lightDiffuse,
		.lpos = this->mShadingData.lightPosition,
		.campos = this->mCam.GetPosition(),
	};

	ubo.proj[1][1] *= -1.0f;

	std::memcpy(this->mFrames[currentImage].uniformBufferMapped, &ubo, sizeof(ubo));
}

#include "HelloTriangleApp/HelloTriangleApplication.hh"
#include "HelloTriangleApplication.hh"

#include "log/loguru.hh"

#include <algorithm>
#include <limits>
#include <map>

std::vector<const char*> HelloTriangleApplication::GetRequiredExtensions() const {
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

	if constexpr(kEnableValidationLayers) {
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	return extensions;
}

HelloTriangleApplication::QueueFamilyIndices HelloTriangleApplication::FindQueueFamilies(vk::PhysicalDevice device) {
	QueueFamilyIndices indices;

	std::vector<vk::QueueFamilyProperties> queueFamilies = device.getQueueFamilyProperties();

	uint32_t i = 0;
	for(const auto& queueFamily : queueFamilies) {
		// If the queue family is able to take graphics commands, save the index of it in the QueueFamilyIndices struct
		if(queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) {
			indices.graphicsFamily = i;
		}

		// Query present support on the current device's graphics queue family
		vk::Bool32 presentSupport = device.getSurfaceSupportKHR(i, this->mSurface);

		// If the device has present support, fill out the corresponding field in the QueueFamilyIndices struct
		if(presentSupport) {
			indices.presentFamily = i;
		}

		if(indices.IsComplete()) break;

		i++;
	}

	return indices;
}

HelloTriangleApplication::SwapChainSupportDetails HelloTriangleApplication::QuerySwapChainSupport(vk::PhysicalDevice device) {
	SwapChainSupportDetails details;

	if(device.getSurfaceCapabilitiesKHR(this->mSurface, &details.capabilities) != vk::Result::eSuccess) [[unlikely]] {
		throw std::runtime_error("Failed to get surface capabilities");
	}

	details.formats = device.getSurfaceFormatsKHR(this->mSurface);
	details.presentModes = device.getSurfacePresentModesKHR(this->mSurface);

	return details;
}

vk::SurfaceFormatKHR HelloTriangleApplication::ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats) {
	// Go through all available formats
	for(const auto& availableFormat : availableFormats) {
		// If the format meets our specs, then use it
		if(availableFormat.format == vk::Format::eR8G8B8A8Srgb && availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
			return availableFormat;
		}
	}

	// Use an arbitrary supported format
	return availableFormats[0];
}

/**
 * @brief Search provided vector for a specific present mode
 * 
 * @param availablePresentModes Vector of available present modes
 * @param mode Present mode to search for
 * @return true Provided present mode was found
 * @return false Provided present mode was not found
 */
static bool IsPresentModePresent(const std::vector<vk::PresentModeKHR>& availablePresentModes, const vk::PresentModeKHR mode) {
	for(const auto& availablePresentMode : availablePresentModes) {
		if(availablePresentMode == mode) {
			return true;
		}
	}
	return false;
}

vk::PresentModeKHR HelloTriangleApplication::ChooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availablePresentModes) {
	if(IsPresentModePresent(availablePresentModes, vk::PresentModeKHR::eMailbox)) {
		LOG_F(6, "%s", "Using mailbox present mode");
		return vk::PresentModeKHR::eMailbox;
	}

	if(IsPresentModePresent(availablePresentModes, vk::PresentModeKHR::eImmediate)) {
		LOG_F(6, "%s", "Using immediate present mode. Visual tearing may occur");
		return vk::PresentModeKHR::eImmediate;
	}

	LOG_F(6, "%s", "Mailbox and immediate present modes not supported! Input lag may be present");
	return vk::PresentModeKHR::eFifo;
}

vk::Extent2D HelloTriangleApplication::ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilities) {
	if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) { // If we already have an extent
		return capabilities.currentExtent;
	} else { // If we need to create an extent
		int width, height;
		glfwGetFramebufferSize(this->mWindow, &width, &height);

		vk::Extent2D actualExtent = {
			.width = static_cast<uint32_t>(width),
			.height = static_cast<uint32_t>(height)
		};

		actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
		actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

		return actualExtent;
	}
}

uint32_t HelloTriangleApplication::FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties) const {
	vk::PhysicalDeviceMemoryProperties memProps = this->mPhysicalDevice.getMemoryProperties();

	for(uint32_t i = 0; i < memProps.memoryTypeCount; i++) {
		if((typeFilter & (1 << i)) && (memProps.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}

	throw std::runtime_error("Failed to find suitable memory type");
}

static int RateDeviceSuitability(const vk::PhysicalDevice& device) {
	// Get device information here
	vk::PhysicalDeviceProperties deviceProperties = device.getProperties();
	vk::PhysicalDeviceFeatures deviceFeatures = device.getFeatures();

	// Rating score of device
	int score = 0;

	// Discrete GPUs are very good!
	if(deviceProperties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu) {
		score += 1000;
	}

	// High image dimensions are also good!
	score += deviceProperties.limits.maxImageDimension2D;

	// Geometry shaders are good too!
	if(deviceFeatures.geometryShader) {
		score += 1000;
	}

	// Long clip distance is good
	score += deviceProperties.limits.maxClipDistances;
	
	return score;
}

void HelloTriangleApplication::PickPhysicalDevice() {
	// Fill out all the devices that can support Vulkan
	std::vector<vk::PhysicalDevice> devices = this->mInstance.enumeratePhysicalDevices();

	// Throw an error if there are no GPUs with Vulkan support
	if(devices.size() == 0) [[unlikely]] {
		throw std::runtime_error("Failed to find GPUs with Vulkan support");
	}

	// Fill out a map of the devices that meet the minimum spec
	std::vector<vk::PhysicalDevice> possibleDevices;

	// See which of the physical meet the minimum spec (supporting a swap chain, etc...)
	for(const auto device : devices) {
		if(this->IsDeviceSuitable(device)) {
			possibleDevices.push_back(device);
		}
	}

	// Use an ordered map to sort candidates by score
	std::multimap<int, vk::PhysicalDevice> candidates;

	// Rate candidates
	for(const auto& device : possibleDevices) {
		int score = RateDeviceSuitability(device);
		candidates.insert(std::make_pair(score, device));
	}

	// Set our best candidate
	if(candidates.rbegin()->first > 0) {
		this->mPhysicalDevice = candidates.rbegin()->second;
	} else [[unlikely]] {
		throw std::runtime_error("Failed to find a suitable GPU!");
	}
}

vk::Format HelloTriangleApplication::FindSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features) const {
	for(auto format : candidates) {
		vk::FormatProperties props;
		this->mPhysicalDevice.getFormatProperties(format, &props);

		if(tiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features) {
			return format;
		} else if(tiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}

	throw std::runtime_error("FindSupportedFormat: Failed to find a supported format");
}

vk::Format HelloTriangleApplication::FindDepthFormat() const {
	return this->FindSupportedFormat({
		vk::Format::eD32Sfloat,
		vk::Format::eD32SfloatS8Uint,
		vk::Format::eD24UnormS8Uint
	}, vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);
}

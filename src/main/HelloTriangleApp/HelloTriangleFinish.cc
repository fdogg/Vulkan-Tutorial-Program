#include "HelloTriangleApplication.hh"

#include "imgui/backends/imgui_impl_glfw.h"
#include "imgui/backends/imgui_impl_vulkan.h"
#include "imgui.h"

#include "utils/DebugUtils.hh"

void HelloTriangleApplication::Cleanup() {
	// Cleanup imgui stuff
	ImGui_ImplVulkan_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	this->CleanupSwapChain();

	this->mDevice.destroySampler(this->mTextureImageSampler);
	this->mDevice.destroyImageView(this->mTextureImageView);

	this->mDevice.destroyImage(this->mTextureImage);
	this->mDevice.freeMemory(this->mTextureImageMemory);

	this->mDevice.destroyPipeline(this->mGraphicsPipeline);
	this->mDevice.destroyPipelineLayout(this->mPipelineLayout);
	this->mDevice.destroyRenderPass(this->mRenderPass);

	for(auto& frame : this->mFrames) {
		vmaUnmapMemory(this->mAllocator, frame.uniformBufferAllocation);
		vmaDestroyBuffer(this->mAllocator, frame.uniformBuffer, frame.uniformBufferAllocation);
	}

	// More imgui stuff
	this->mDevice.destroyDescriptorPool(this->mImguiDescriptorPool);

	this->mDevice.destroyDescriptorPool(this->mDescriptorPool);

	this->mDevice.destroyDescriptorSetLayout(this->mDescriptorSetLayout);

	vmaDestroyBuffer(this->mAllocator, this->mIndexBuffer, this->mIndexBufferAllocation);

	vmaDestroyBuffer(this->mAllocator, this->mVertexBuffer, this->mVertBufferAllocation);

	for(auto& frame : this->mFrames) {
		this->mDevice.destroySemaphore(frame.imgAvailableSemaphore);
		this->mDevice.destroySemaphore(frame.renderFinishedSemaphore);
		this->mDevice.destroyFence(frame.inFlightFence);
	}

	vmaDestroyAllocator(this->mAllocator);

	this->mDevice.destroyCommandPool(this->mCommandPool);

	this->mDevice.destroy();

	if constexpr(kEnableValidationLayers) {
		DestroyDebugUtilsMessengerEXT(this->mInstance, this->mDebugMessenger, nullptr);
	}

	// Cleanup Vulkan
	this->mInstance.destroySurfaceKHR(this->mSurface);
	this->mInstance.destroy();

	// Cleanup glfw
	glfwDestroyWindow(this->mWindow);
	glfwTerminate();
}

void HelloTriangleApplication::CleanupSwapChain() {
	this->mDevice.destroyImageView(this->mDepthImageView);
	this->mDevice.destroyImage(this->mDepthImage);
	this->mDevice.freeMemory(this->mDepthImageMemory);

	// Destroy framebuffers
	for(auto framebuffer : this->mSwapChainFramebuffers) {
		this->mDevice.destroyFramebuffer(framebuffer);
	}

	// Destroy image views
	for(auto& element : this->mSwapChainImages) {
		this->mDevice.destroyImageView(element.view);
	}

	// Destroy swapchains
	this->mDevice.destroySwapchainKHR(this->mSwapChain);
}
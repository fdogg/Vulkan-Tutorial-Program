#include "Constants.hh"
#include "HelloTriangleApplication.hh"

#include "stb_image.h"

#include "log/loguru.hh"
#include <vulkan/vulkan_enums.hpp>

void HelloTriangleApplication::CreateSwapChain() {
	// Get the supported formats and present modes of the current device
	SwapChainSupportDetails swapChainSupport = this->QuerySwapChainSupport(this->mPhysicalDevice);

	// Pick a format who's format is RGBA8 and color space is nonlinear
	vk::SurfaceFormatKHR surfaceFormat = this->ChooseSwapSurfaceFormat(swapChainSupport.formats);
	// Pick a present mode
	vk::PresentModeKHR presentMode = this->ChooseSwapPresentMode(swapChainSupport.presentModes);
	// Get the size of the swapchain images
	vk::Extent2D extent = this->ChooseSwapExtent(swapChainSupport.capabilities);

	uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if(swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
		imageCount = swapChainSupport.capabilities.maxImageCount;
	}

	// Fill out the swapchain create info
	vk::SwapchainCreateInfoKHR createInfo {
		.surface = this->mSurface,
		.minImageCount = imageCount,
		.imageFormat = surfaceFormat.format,
		.imageColorSpace = surfaceFormat.colorSpace,
		.imageExtent = extent,
		.imageArrayLayers = 1,
		.imageUsage = vk::ImageUsageFlagBits::eColorAttachment,
		.preTransform = swapChainSupport.capabilities.currentTransform,
		.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque,
		.presentMode = presentMode,
		.clipped = true,
		.oldSwapchain = nullptr
	};

	// Specify the indices of queue families we will be using for drawing and presenting
	QueueFamilyIndices indices = this->FindQueueFamilies(this->mPhysicalDevice);
	uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

	// If the graphics family and queue family are not the same
	if(indices.graphicsFamily != indices.presentFamily) { // Specify the different queue families we are using
		createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	} else { // Assume (not really assume because Vulkan knows) that the graphcis and present queues are the same
		createInfo.imageSharingMode = vk::SharingMode::eExclusive;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}

	vk::Result result = this->mDevice.createSwapchainKHR(&createInfo, nullptr, &this->mSwapChain);
	if(result != vk::Result::eSuccess) [[unlikely]] {
		throw std::runtime_error("Failed to create swapchain: got vk::Result " + vk::to_string(result));
	}

	// Get the swapchain images
	std::vector<vk::Image> images = this->mDevice.getSwapchainImagesKHR(this->mSwapChain);

	this->mSwapChainImages.reserve(images.size());

	for(auto& image : images) {
		this->mSwapChainImages.push_back({ std::move(image), { } });
	}

	this->mSwapChainImageFormat = surfaceFormat.format;
	this->mSwapChainExtent = extent;
}

void HelloTriangleApplication::CreateImageViews() {
	for(auto& element : this->mSwapChainImages) {
		element.view = this->CreateImageView(element.image, this->mSwapChainImageFormat, vk::ImageAspectFlagBits::eColor);
	}
}

void HelloTriangleApplication::CreateFramebuffers() {
	size_t swapChainImageViewsSize = this->mSwapChainImages.size();

	this->mSwapChainFramebuffers.resize(swapChainImageViewsSize);

	for(size_t i = 0; i < swapChainImageViewsSize; i++) {
		std::array<vk::ImageView, 2> attachments {
			this->mSwapChainImages[i].view,
			this->mDepthImageView
		};

		vk::FramebufferCreateInfo createInfo {
			.renderPass = this->mRenderPass,
			.attachmentCount = static_cast<uint32_t>(attachments.size()),
			.pAttachments = attachments.data(),
			.width = this->mSwapChainExtent.width,
			.height = this->mSwapChainExtent.height,
			.layers = 1
		};

		if(this->mDevice.createFramebuffer(&createInfo, nullptr, &this->mSwapChainFramebuffers[i]) != vk::Result::eSuccess) [[unlikely]] {
			throw std::runtime_error("Failed to create framebuffer");
		}
	}
}

void HelloTriangleApplication::CreateImage(
	uint32_t width, uint32_t height,
	vk::Format format, vk::ImageTiling tiling,
	vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties,
	vk::Image& image, vk::DeviceMemory& imageMemory
) {

	vk::ImageCreateInfo createInfo {
		.imageType = vk::ImageType::e2D,
		.format = format,
		.extent {
			.width = static_cast<uint32_t>(width),
			.height = static_cast<uint32_t>(height),
			.depth = 1
		},
		.mipLevels = 1,
		.arrayLayers = 1,
		.samples = vk::SampleCountFlagBits::e1,
		.tiling = tiling,
		.usage = usage,
		.sharingMode = vk::SharingMode::eExclusive,
		.initialLayout = vk::ImageLayout::eUndefined
	};

	image = this->mDevice.createImage(createInfo);

	vk::MemoryRequirements memRequirements = this->mDevice.getImageMemoryRequirements(image);

	vk::MemoryAllocateInfo allocInfo {
		.allocationSize = memRequirements.size,
		.memoryTypeIndex = this->FindMemoryType(memRequirements.memoryTypeBits, properties),
	};

	imageMemory = this->mDevice.allocateMemory(allocInfo);
	this->mDevice.bindImageMemory(image, imageMemory, 0);
}

void HelloTriangleApplication::CreateTextureImage() {
	/* Load texture image */
	int texWidth, texHeight, texChannels;
	stbi_uc *pixels = stbi_load(kModelTexturePath.data(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);

	if(!pixels) [[unlikely]] throw std::runtime_error("Failed to load statue image");

	LOG_F(INFO, "Successfully loaded texture image [%s]", kModelTexturePath.data());

	vk::DeviceSize imageSize = texWidth * texHeight * 4;

	vk::Buffer stagingBuffer;
	VmaAllocation stagingBufferAllocation;

	vk::BufferCreateInfo stagingBuffInfo {
		.size = imageSize,
		.usage = vk::BufferUsageFlagBits::eTransferSrc,
		.sharingMode = vk::SharingMode::eExclusive
	};

	VmaAllocationCreateInfo allocInfo { };
	allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

	if(vmaCreateBuffer(
		this->mAllocator,
		(const VkBufferCreateInfo*)&stagingBuffInfo,
		&allocInfo,
		(VkBuffer*)&stagingBuffer,
		&stagingBufferAllocation,
		nullptr
	) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Unable to alloc staging buffer");

	void *data;

	if(vmaMapMemory(this->mAllocator, stagingBufferAllocation, &data) != VK_SUCCESS) [[unlikely]] throw std::runtime_error("Failed to map memory!");

	std::memcpy(data, pixels, static_cast<size_t>(imageSize));

	vmaUnmapMemory(this->mAllocator, stagingBufferAllocation);

	stbi_image_free(pixels);

	this->CreateImage(
		texWidth, texHeight,
		vk::Format::eR8G8B8A8Srgb, vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
		vk::MemoryPropertyFlagBits::eDeviceLocal,
		this->mTextureImage, this->mTextureImageMemory
	);

	this->TransitionImageLayout(
		this->mTextureImage, vk::Format::eR8G8B8A8Srgb,
		vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal
	);

	this->CopyBufferToImage(
		stagingBuffer, this->mTextureImage,
		static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight)
	);

	this->TransitionImageLayout(
		this->mTextureImage, vk::Format::eR8G8B8A8Srgb,
		vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal
	);

	vmaDestroyBuffer(this->mAllocator, stagingBuffer, stagingBufferAllocation);
}

void HelloTriangleApplication::TransitionImageLayout(
	vk::Image image, vk::Format format,
	vk::ImageLayout oldLayout, vk::ImageLayout newLayout
) {

	// Stop compiler complaining about unused variables
	(void)format;
	
	vk::CommandBuffer commandBuffer = this->BeginSingleTimeCommands();
	{
		vk::ImageMemoryBarrier barrier {
			.srcAccessMask = { }, // TODO
			.dstAccessMask = { }, // TODO
			.oldLayout = oldLayout,
			.newLayout = newLayout,
			.srcQueueFamilyIndex = vk::QueueFamilyIgnored,
			.dstQueueFamilyIndex = vk::QueueFamilyIgnored,
			.image = image,
			.subresourceRange {
				.aspectMask = vk::ImageAspectFlagBits::eColor,
				.baseMipLevel = 0,
				.levelCount = 1,
				.baseArrayLayer = 0,
				.layerCount = 1
			},
		};

		vk::PipelineStageFlags sourceStage;
		vk::PipelineStageFlags destinationStage;

		if(oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal) {
			barrier.srcAccessMask = { };
			barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

			sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
			destinationStage = vk::PipelineStageFlagBits::eTransfer;
		} else if(oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal) {
			barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
			barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

			sourceStage = vk::PipelineStageFlagBits::eTransfer;
			destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
		} else if(oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal) {
			barrier.srcAccessMask = { };
			barrier.dstAccessMask =
				vk::AccessFlagBits::eDepthStencilAttachmentRead |
				vk::AccessFlagBits::eDepthStencilAttachmentWrite;

			sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
			destinationStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
		} else [[unlikely]] {
			throw std::invalid_argument("Unsupported layout transition");
		}

		if(newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal) {
			barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;

			if(this->HasStencilComponent(format)) {
				barrier.subresourceRange.aspectMask |= vk::ImageAspectFlagBits::eStencil;
			}
		}

		commandBuffer.pipelineBarrier(
			sourceStage, destinationStage,
			{ },
			{ },
			{ },
			barrier
		);
	}
	this->EndSingleTimeCommandBuffer(commandBuffer);
}

vk::ImageView HelloTriangleApplication::CreateImageView(vk::Image image, vk::Format format, vk::ImageAspectFlagBits aspectFlags) {
	vk::ImageViewCreateInfo viewInfo {
		.image = image,
		.viewType = vk::ImageViewType::e2D,
		.format = format,
		.subresourceRange = {
			.aspectMask = aspectFlags,
			.baseMipLevel = 0,
			.levelCount = 1,
			.baseArrayLayer = 0,
			.layerCount = 1
		}
	};

	vk::ImageView imageView = this->mDevice.createImageView(viewInfo);

	return imageView;
}

void HelloTriangleApplication::CreateTextureImageView() {
	this->mTextureImageView = this->CreateImageView(this->mTextureImage, vk::Format::eR8G8B8A8Srgb, vk::ImageAspectFlagBits::eColor);
}

void HelloTriangleApplication::CreateTextureSampler() {
	vk::SamplerCreateInfo samplerInfo {
		.magFilter = vk::Filter::eLinear,
		.minFilter = vk::Filter::eLinear,
		.mipmapMode = vk::SamplerMipmapMode::eLinear,
		.addressModeU = vk::SamplerAddressMode::eRepeat,
		.addressModeV = vk::SamplerAddressMode::eRepeat,
		.addressModeW = vk::SamplerAddressMode::eRepeat,
		.mipLodBias = 0.0f,
		.anisotropyEnable = true,
		.maxAnisotropy = this->mPhysicalDevice.getProperties().limits.maxSamplerAnisotropy,
		.compareEnable = false,
		.compareOp = vk::CompareOp::eAlways,
		.minLod = 0.0f,
		.maxLod = 0.0f,
		.borderColor = vk::BorderColor::eIntOpaqueBlack,
		.unnormalizedCoordinates = false
	};

	this->mTextureImageSampler = this->mDevice.createSampler(samplerInfo);
}

void HelloTriangleApplication::CreateDepthResource() {
	vk::Format depthFormat = this->FindDepthFormat();

	this->CreateImage(
		this->mSwapChainExtent.width, this->mSwapChainExtent.height,
		depthFormat, vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eDepthStencilAttachment,
		vk::MemoryPropertyFlagBits::eDeviceLocal,
		this->mDepthImage, this->mDepthImageMemory
	);

	this->mDepthImageView = this->CreateImageView(this->mDepthImage, depthFormat, vk::ImageAspectFlagBits::eDepth);

	this->TransitionImageLayout(this->mDepthImage, depthFormat, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal);
}

#include "Constants.hh"
#include "HelloTriangleApp/HelloTriangleApplication.hh"

#include "imgui.h"
#include "imgui/backends/imgui_impl_glfw.h"
#include "imgui/backends/imgui_impl_vulkan.h"

#include "log/loguru.hh"

#include <chrono>

namespace stdchr = std::chrono;

void HelloTriangleApplication::InitImgui() {
	// Create descriptor pools for ImGui
	std::array<vk::DescriptorPoolSize, 1> poolSizes {
		{{ vk::DescriptorType::eCombinedImageSampler, 1 }}
	};

	vk::DescriptorPoolCreateInfo poolInfo {
		.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
		.maxSets = static_cast<uint32_t>(kMaxFramesInFlight),
		.poolSizeCount = static_cast<uint32_t>(poolSizes.size()),
		.pPoolSizes = poolSizes.data()
	};

	this->mImguiDescriptorPool = this->mDevice.createDescriptorPool(poolInfo);

	// ImGui setup stuff

	IMGUI_CHECKVERSION();

	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForVulkan(this->mWindow, true);

	ImGui_ImplVulkan_InitInfo imInitInfo {
		.Instance = this->mInstance,
		.PhysicalDevice = this->mPhysicalDevice,
		.Device = this->mDevice,
		.QueueFamily = this->FindQueueFamilies(this->mPhysicalDevice).graphicsFamily.value(),
		.Queue = this->mGraphicsQueue,
		.PipelineCache = nullptr,
		.DescriptorPool = this->mImguiDescriptorPool,
		.Subpass = 0,
		.MinImageCount = 2,
		.ImageCount = 2,
		.MSAASamples = VK_SAMPLE_COUNT_1_BIT,
		.UseDynamicRendering = false,
		.ColorAttachmentFormat = VK_FORMAT_UNDEFINED,
		.Allocator = nullptr,
		.CheckVkResultFn = nullptr,
		.MinAllocationSize = 0
	};
	ImGui_ImplVulkan_Init(&imInitInfo, this->mRenderPass);

	// Used to upload fonts, not anymore

	ImGui_ImplVulkan_SetMinImageCount(kMaxFramesInFlight);
}

void HelloTriangleApplication::ImguiDrawing() {
	// Drawing loop
	ImGui_ImplVulkan_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	ImGui::Begin("Application Data");
		ImGui::SeparatorText("Options");
		ImGui::PushItemWidth(64);
		glm::vec3 camPos = this->mCam.GetPosition();
		ImGui::Text("Position: [%.2f, %.2f, %.2f]", camPos.x, camPos.y, camPos.z);

		static auto startTime = stdchr::high_resolution_clock::now();
		static float initialRot = 0.0f;
		static bool rotateEnabled = false;

		ImGui::BeginDisabled(rotateEnabled);
		ImGui::DragFloat("Rotation", &this->mModelRotationDegs, 1.0f, 0.0f, 0.0f, "%.0f");
		ImGui::EndDisabled();

		if(ImGui::Checkbox("Rotate", &rotateEnabled) == true) /* Rotate button is first clicked */ {
			startTime = stdchr::high_resolution_clock::now();
			initialRot = this->mModelRotationDegs;
		}
		if(rotateEnabled) /* While rotate is enabled */ {
			const auto currentTime = stdchr::high_resolution_clock::now();
			this->mModelRotationDegs = stdchr::duration<float, stdchr::seconds::period>(currentTime - startTime).count() * 20.0f + initialRot;
		}

		ImGui::DragFloat("Size", &this->mModelZoom, 0.1f, 0.5f, 5.0f, "%.2f");
		ImGui::PopItemWidth();

		static bool demoWindow = false;

		ImGui::Checkbox("ImGui Demo Window", &demoWindow);

		if(ImGui::CollapsingHeader("Shading")) /* Shading information */ {
			ImGui::SeparatorText("Material");
			ImGui::ColorEdit3("MatSpecular", &this->mShadingData.matSpecular[0]);
			ImGui::ColorEdit3("MatDiffuse", &this->mShadingData.matDiffuse[0]);
			ImGui::DragFloat("Ambient Factor", &this->mShadingData.ambient, 0.2f, 0.0f, 2.0f);
			ImGui::DragFloat("Shininess", &this->mShadingData.shininess, 0.05f, 0.0f, 100.0f);
			ImGui::SeparatorText("Light");
			ImGui::ColorEdit3("LightSpecular", &this->mShadingData.lightSpecular[0]);
			ImGui::ColorEdit3("LightDiffuse", &this->mShadingData.lightDiffuse[0]);
			ImGui::DragFloat3("Position", &this->mShadingData.lightPosition[0], 0.05f);
		}

		// Controls information
		ImGui::SeparatorText("Controls");
		ImGui::BulletText("Move - W, A, S, D");
		ImGui::BulletText("Up/Down - E, Q");
		ImGui::BulletText("Toggle Cursor / Fly Mode - R");
		ImGui::BulletText("Exit - ESC");
	ImGui::End();
	if(demoWindow) ImGui::ShowDemoWindow(&demoWindow);
}

void HelloTriangleApplication::ImguiRender(vk::CommandBuffer& cmd) {
	ImGui::Render();
	ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), cmd);
}

#include "HelloTriangleApplication.hh"

#include "log/loguru.hh"

void HelloTriangleApplication::Run() {
	this->InitWindow();
	this->InitVulkan();
	this->InitImgui();
	this->MainLoop();
	this->Cleanup();
}

void HelloTriangleApplication::InitWindow() {
	glfwInit();

	// Tell GLFW to not use OpenGL
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	// Tell GLFW to let us resize the window 
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

	// Initialize the window
	this->mWindow = glfwCreateWindow(kWindowWidth, kWindowHeight, "Vulkan", nullptr, nullptr);

	// Set the window user pointer to the applications address
	glfwSetWindowUserPointer(this->mWindow, reinterpret_cast<void*>(this));

	// Set the callback for resizing
	glfwSetFramebufferSizeCallback(this->mWindow, FramebufferResizeCallback);

	// Key input
	glfwSetKeyCallback(this->mWindow, KeyCallback);

	// Mouse movement
	glfwSetCursorPosCallback(this->mWindow, MouseCallback);
}

void HelloTriangleApplication::InitVulkan() {
	this->CreateInstance();
	this->SetupDebugMessenger();
	this->CreateSurface();
	this->PickPhysicalDevice();
	this->CreateLogicalDevice();
	this->CreateAllocator();
	this->CreateSwapChain();
	this->CreateImageViews();
	this->CreateRenderPass();
	this->CreateDescriptorSetLayout();
	this->CreateGraphicsPipeline();
	this->CreateCommandPool();
	this->CreateDepthResource();
	this->CreateFramebuffers();
	this->CreateTextureImage();
	this->CreateTextureImageView();
	this->CreateTextureSampler();
	this->LoadModel();
	this->CreateVertexBuffer();
	this->CreateIndexBuffer();
	this->CreateUniformBuffers();
	this->CreateDescriptorPool();
	this->CreateDescriptorSets();
	this->CreateCommandBuffers();
	this->CreateSyncObjects();
}

#include "HelloTriangleApplication.hh"

#include <tiny_obj_loader.h>
#include <log/loguru.hh>
#include <unordered_map>

void HelloTriangleApplication::LoadModel() {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warning, error;

	if(!tinyobj::LoadObj(&attrib, &shapes, &materials, &warning, &error, kModelPath.data())) [[unlikely]] {
		throw std::runtime_error(warning + error);
	}

	// Reserve a chunk memory to try and speed up the loading into memory
	this->mVertices.reserve(shapes.size() * 3);
	this->mIndices.reserve(shapes.size() * 3);

	std::unordered_map<Vertex, uint32_t> uniqueVerts { };

	uniqueVerts.reserve(shapes.size() / 2ULL);

	for(const auto& shape : shapes) {
		for(const auto& index : shape.mesh.indices) {
			Vertex vtx {
				.position = {
					attrib.vertices[3 * index.vertex_index + 0],
					attrib.vertices[3 * index.vertex_index + 1],
					attrib.vertices[3 * index.vertex_index + 2]
				},
				.color = { 1.0f, 1.0f, 1.0f },
				.texCoord = {
					attrib.texcoords[2 * index.texcoord_index + 0],
					1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
				},
				.normal = {
					attrib.normals[3 * index.normal_index + 0],
					attrib.normals[3 * index.normal_index + 1],
					attrib.normals[3 * index.normal_index + 2]
				}
			};

			if(uniqueVerts.count(vtx) == 0) {
				uniqueVerts[vtx] = static_cast<uint32_t>(this->mVertices.size());
				this->mVertices.push_back(vtx);
			}

			this->mIndices.push_back(uniqueVerts[vtx]);
		}
	}

	LOG_F(
		INFO, "Successfully loaded model [%s], %lld verts, %lld indices",
		kModelPath.data(), this->mVertices.size(), this->mIndices.size()
	);
}

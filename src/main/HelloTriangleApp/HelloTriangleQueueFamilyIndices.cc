#include "HelloTriangleApplication.hh"

bool HelloTriangleApplication::QueueFamilyIndices::IsComplete() const noexcept {
	bool complete = true;
	complete &= graphicsFamily.has_value();
	complete &= presentFamily.has_value();
	return complete;
}

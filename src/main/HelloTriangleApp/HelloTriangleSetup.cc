#include "HelloTriangleApplication.hh"

#include "utils/FileUtils.hh"
#include "log/loguru.hh"

#include <stdexcept>
#include <set>

void HelloTriangleApplication::CreateInstance() {
	// Make sure validation layers are available if we would like them
	if(kEnableValidationLayers && !this->CheckValidationLayerSupport()) [[unlikely]] {
		throw std::runtime_error("Validation layers requested, but not available");
	}

	/** @brief Basic Vulkan application information */
	vk::ApplicationInfo appInfo {
		.pApplicationName = "Vulkan Tutorial",
		.applicationVersion = VK_API_VERSION_1_0,
		.pEngineName = "No Engine",
		.engineVersion = VK_API_VERSION_1_0,
		.apiVersion = VK_API_VERSION_1_3,
	};

	// Code to get the extensions required by GLFW
	uint32_t glfwExtensionCount = 0;
	const char **glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	if constexpr(kDebug) {
		// Display all extensions required by GLFW
		LOG_SCOPE_F(1, "Extensions required by GLFW");
		for(uint32_t i = 0; i < glfwExtensionCount; i++) {
			LOG_F(1, "%s", glfwExtensions[i]);
		}
	}

	/** @brief Information about the instance */
	vk::InstanceCreateInfo createInfo {
		.pApplicationInfo = &appInfo,
		.enabledExtensionCount = glfwExtensionCount,
		.ppEnabledExtensionNames = glfwExtensions
	};

	vk::DebugUtilsMessengerCreateInfoEXT debugInfo { };

	if constexpr(kEnableValidationLayers) {
		createInfo.enabledLayerCount = static_cast<uint32_t>(kValidationLayers.size());
		createInfo.ppEnabledLayerNames = kValidationLayers.data();

		this->PopulateDebugMessengerCreateInfo(debugInfo);
		createInfo.pNext = &debugInfo;
	}

	std::vector<vk::ExtensionProperties> availableExtensions = vk::enumerateInstanceExtensionProperties(nullptr);

	if constexpr(kDebug) {
		// Display all available extensions
		LOG_SCOPE_F(1, "Available extensions");

		for(const auto& extension : availableExtensions) {
			LOG_F(1, "%s", extension.extensionName.data());
		}
	}

	std::vector<const char*> extensions = this->GetRequiredExtensions();
	createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	createInfo.ppEnabledExtensionNames = extensions.data();

	this->mInstance = vk::createInstance(createInfo, nullptr);
}

void HelloTriangleApplication::CreateSurface() {
	if(glfwCreateWindowSurface(this->mInstance, this->mWindow, nullptr, reinterpret_cast<VkSurfaceKHR*>(&this->mSurface)) != VK_SUCCESS) [[unlikely]] {
		throw std::runtime_error("Failed to create a window surface");
	}
}

void HelloTriangleApplication::CreateLogicalDevice() {
	// FInd the indices
	QueueFamilyIndices indices = this->FindQueueFamilies(this->mPhysicalDevice);

	// Specify the queues to be created
	std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
	// Specify the queue families we want to create on
	std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

	/*
	 * The currently available drivers will only allow you to create a small number of
	 * queues for each queue family and you don’t really need more than one. That’s
	 * because you can create all of the command buffers on multiple threads and then
	 * submit them all at once on the main thread with a single low-overhead call
	*/

	// Number of queues to create for each queue family
	const uint32_t queueCount = 1;

	/*
	 * Vulkan lets you assign priorities to queues to influence the scheduling of command
	 * buffer execution using floating point numbers between 0.0 and 1.0. This
	 * is required even if there is only a single queue
	*/
	const float queuePriority = 1.0f;
	for(uint32_t queueFamilyIdx : uniqueQueueFamilies) {
		vk::DeviceQueueCreateInfo queueCreateInfo {
			.queueFamilyIndex = queueFamilyIdx,
			.queueCount = queueCount,
			.pQueuePriorities = &queuePriority
		};
		queueCreateInfos.push_back(queueCreateInfo);
	}

	// Specify the device features we want
	vk::PhysicalDeviceFeatures deviceFeatures {
		.samplerAnisotropy = true
	};

	// Fill out the create info
	vk::DeviceCreateInfo createInfo {
		.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size()),
		.pQueueCreateInfos = queueCreateInfos.data(),
		.enabledExtensionCount = static_cast<uint32_t>(kDeviceExtensions.size()),
		.ppEnabledExtensionNames = kDeviceExtensions.data(),
		.pEnabledFeatures = &deviceFeatures
	};
	
	// Add validation layers to the device layers if we have enabled them
	if constexpr(kEnableValidationLayers) {
		createInfo.setEnabledLayerCount(static_cast<uint32_t>(kValidationLayers.size()));
		createInfo.setPpEnabledLayerNames(kValidationLayers.data());
	}

	this->mDevice = this->mPhysicalDevice.createDevice(createInfo, nullptr);

	this->mGraphicsQueue = this->mDevice.getQueue(indices.graphicsFamily.value(), 0);
	this->mPresentQueue = this->mDevice.getQueue(indices.presentFamily.value(), 0);
}

vk::ShaderModule HelloTriangleApplication::CreateShaderModule(const std::vector<char>& code) {
	vk::ShaderModuleCreateInfo createInfo {
		.codeSize = code.size(),
		.pCode = reinterpret_cast<const uint32_t*>(code.data())
	};

	return this->mDevice.createShaderModule(createInfo);
}

void HelloTriangleApplication::CreateGraphicsPipeline() {
	// Load vertex shader
	auto vertShaderCode = utils::ReadFile("shaders/main.vert.spv");
	LOG_F(INFO, "%s", "Successfully loaded vertex shader");
	// Load fragment shader
	auto fragShaderCode = utils::ReadFile("shaders/main.frag.spv");
	LOG_F(INFO, "%s", "Successfully loaded fragment shader");

#if 0
	// Debug info
	std::cout << "Shaders have been loaded!\n"
	<< "\tVert shader size: " << vertShaderCode.size() << " bytes\n"
	<< "\tFrag shader size: " << fragShaderCode.size() << " bytes" << std::endl;
#endif

	// Vulkan vertex shader module
	vk::ShaderModule vertShaderModule = this->CreateShaderModule(vertShaderCode);
	// Vulkan fragment shader module
	vk::ShaderModule fragShaderModule = this->CreateShaderModule(fragShaderCode);

	vk::PipelineShaderStageCreateInfo vertShaderInfo {
		.stage = vk::ShaderStageFlagBits::eVertex,
		.module = vertShaderModule,
		.pName = "main",
		.pSpecializationInfo = nullptr
	};

	vk::PipelineShaderStageCreateInfo fragShaderInfo {
		.stage = vk::ShaderStageFlagBits::eFragment,
		.module = fragShaderModule,
		.pName = "main",
		.pSpecializationInfo = nullptr
	};

	vk::PipelineShaderStageCreateInfo shaderStages[] = { vertShaderInfo, fragShaderInfo };

	auto bindingDescription = Vertex::GetBindingDescription();
	auto attributeDescriptions = Vertex::GetAttributeDescriptions();

	vk::PipelineVertexInputStateCreateInfo vertexInputInfo {
		.vertexBindingDescriptionCount = 1,
		.pVertexBindingDescriptions = &bindingDescription,
		.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size()),
		.pVertexAttributeDescriptions = attributeDescriptions.data()
	};

	vk::PipelineInputAssemblyStateCreateInfo inputAssembly {
		.topology = vk::PrimitiveTopology::eTriangleList,
		.primitiveRestartEnable = false
	};

	vk::PipelineViewportStateCreateInfo viewportState {
		.viewportCount = 1,
		.scissorCount = 1
	};

	vk::PipelineRasterizationStateCreateInfo rasterizer {
		.depthClampEnable = false,
		.rasterizerDiscardEnable = false,
		.polygonMode = vk::PolygonMode::eFill,
		.cullMode = vk::CullModeFlagBits::eBack,
		.frontFace = vk::FrontFace::eCounterClockwise,
		.depthBiasEnable = false,
		.depthBiasConstantFactor = 0.0f,
		.depthBiasClamp = 0.0f,
		.depthBiasSlopeFactor = 0.0f,
		.lineWidth = 1.0f
	};

	vk::PipelineMultisampleStateCreateInfo multisampling {
		.rasterizationSamples = vk::SampleCountFlagBits::e1,
		.sampleShadingEnable = false,
		.minSampleShading = 1.0f,
		.pSampleMask = nullptr,
		.alphaToCoverageEnable = false,
		.alphaToOneEnable = false
	};

	// Depth and stencil testing should go here
	vk::PipelineColorBlendAttachmentState colorBlendAttachment {
		.blendEnable = false,
		.srcColorBlendFactor = vk::BlendFactor::eOne,
		.dstColorBlendFactor = vk::BlendFactor::eZero,
		.colorBlendOp = vk::BlendOp::eAdd,
		.srcAlphaBlendFactor = vk::BlendFactor::eOne,
		.dstAlphaBlendFactor = vk::BlendFactor::eZero,
		.alphaBlendOp = vk::BlendOp::eAdd,
		.colorWriteMask =
			vk::ColorComponentFlagBits::eR |
			vk::ColorComponentFlagBits::eG |
			vk::ColorComponentFlagBits::eB |
			vk::ColorComponentFlagBits::eA
	};

	vk::PipelineColorBlendStateCreateInfo colorBlending {
		.logicOpEnable = false,
		.logicOp = vk::LogicOp::eCopy,
		.attachmentCount = 1,
		.pAttachments = &colorBlendAttachment,
		.blendConstants = {{ 0.0f, 0.0f, 0.0f, 0.0f }},
	};

	// List of states that can be updated post-pipeline creation while program is running
	const std::vector<vk::DynamicState> kDynamicStates = {
		vk::DynamicState::eViewport,
		vk::DynamicState::eScissor
	};

	vk::PipelineDynamicStateCreateInfo dynamicState {
		.dynamicStateCount = static_cast<uint32_t>(kDynamicStates.size()),
		.pDynamicStates = kDynamicStates.data()
	};

	vk::PipelineLayoutCreateInfo pipelineLayoutInfo {
		.setLayoutCount = 1,
		.pSetLayouts = &this->mDescriptorSetLayout,
		.pushConstantRangeCount = 0,
		.pPushConstantRanges = nullptr
	};

	this->mPipelineLayout = this->mDevice.createPipelineLayout(pipelineLayoutInfo);

	vk::PipelineDepthStencilStateCreateInfo depthStencil {
		.depthTestEnable = true,
		.depthWriteEnable = true,
		.depthCompareOp = vk::CompareOp::eLess,

		.depthBoundsTestEnable = false,
		.stencilTestEnable = false,

		.front = { },
		.back = { },

		.minDepthBounds = 0.0f,
		.maxDepthBounds = 1.0f,
	};

	vk::GraphicsPipelineCreateInfo pipelineInfo {
		.stageCount = 2,
		.pStages = shaderStages,

		.pVertexInputState = &vertexInputInfo,
		.pInputAssemblyState = &inputAssembly,
		.pViewportState = &viewportState,
		.pRasterizationState = &rasterizer,
		.pMultisampleState = &multisampling,
		.pDepthStencilState = &depthStencil,
		.pColorBlendState = &colorBlending,
		.pDynamicState = &dynamicState,

		.layout = this->mPipelineLayout,
		.renderPass = this->mRenderPass,
		.subpass = 0,
		.basePipelineHandle = nullptr,
		.basePipelineIndex = -1
	};

	// Make the pipeline
	vk::ResultValue<vk::Pipeline> pipelineResult = this->mDevice.createGraphicsPipeline(nullptr, pipelineInfo);
	if(pipelineResult.result != vk::Result::eSuccess) [[unlikely]] {
		throw std::runtime_error("Failed to create graphics pipeline");
	}

	this->mGraphicsPipeline = pipelineResult.value;

	this->mDevice.destroyShaderModule(vertShaderModule);
	this->mDevice.destroyShaderModule(fragShaderModule);
}

void HelloTriangleApplication::CreateRenderPass() {
	// Color attachment description for render pass
	vk::AttachmentDescription colorAttachment {
		.format = this->mSwapChainImageFormat,
		.samples = vk::SampleCountFlagBits::e1,
		.loadOp = vk::AttachmentLoadOp::eClear,
		.storeOp = vk::AttachmentStoreOp::eStore,
		.stencilLoadOp = vk::AttachmentLoadOp::eClear,
		.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
		.initialLayout = vk::ImageLayout::eUndefined,
		.finalLayout = vk::ImageLayout::ePresentSrcKHR
	};

	// Specify color attachment reference
	vk::AttachmentReference colorAttachmentRef {
		.attachment = 0,
		.layout = vk::ImageLayout::eColorAttachmentOptimal
	};

	// Depth buffer attachment
	vk::AttachmentDescription depthAttachment {
		.format = this->FindDepthFormat(),
		.samples = vk::SampleCountFlagBits::e1,
		.loadOp = vk::AttachmentLoadOp::eClear,
		.storeOp = vk::AttachmentStoreOp::eDontCare,
		.stencilLoadOp = vk::AttachmentLoadOp::eDontCare,
		.stencilStoreOp = vk::AttachmentStoreOp::eDontCare,
		.initialLayout = vk::ImageLayout::eUndefined,
		.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal
	};

	vk::AttachmentReference depthAttachmentRef {
		.attachment = 1,
		.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal
	};

	// Specify subpass description
	vk::SubpassDescription subpass {
		.colorAttachmentCount = 1,
		.pColorAttachments = &colorAttachmentRef,
		.pDepthStencilAttachment = &depthAttachmentRef
	};

	vk::SubpassDependency dependency {
		.srcSubpass = vk::SubpassExternal,
		.dstSubpass = 0,

		.srcStageMask = 
			vk::PipelineStageFlagBits::eColorAttachmentOutput |
			vk::PipelineStageFlagBits::eEarlyFragmentTests,
		.dstStageMask = 
			vk::PipelineStageFlagBits::eColorAttachmentOutput |
			vk::PipelineStageFlagBits::eEarlyFragmentTests,

		.srcAccessMask = vk::AccessFlagBits::eNone,

		.dstAccessMask =
			vk::AccessFlagBits::eColorAttachmentWrite |
			vk::AccessFlagBits::eDepthStencilAttachmentWrite
	};

	std::array<vk::AttachmentDescription, 2> attachments { colorAttachment, depthAttachment };

	vk::RenderPassCreateInfo renderPassInfo {
		.attachmentCount = static_cast<uint32_t>(attachments.size()),
		.pAttachments = attachments.data(),
		.subpassCount = 1,
		.pSubpasses = &subpass,

		.dependencyCount = 1,
		.pDependencies = &dependency
	};

	this->mRenderPass = this->mDevice.createRenderPass(renderPassInfo);
}

void HelloTriangleApplication::CreateCommandPool() {
	QueueFamilyIndices queueFamilyIndices = this->FindQueueFamilies(this->mPhysicalDevice);

	vk::CommandPoolCreateInfo createInfo {
		.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
		.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value()
	};

	this->mCommandPool = this->mDevice.createCommandPool(createInfo);
}

void HelloTriangleApplication::CreateSyncObjects() {
	vk::SemaphoreCreateInfo semaphoreInfo { };

	vk::FenceCreateInfo fenceInfo {
		.flags = vk::FenceCreateFlagBits::eSignaled
	};

	// Create each semaphore and fence used by the frames
	for(auto& frame : this->mFrames) {
		try {
			frame.imgAvailableSemaphore = this->mDevice.createSemaphore(semaphoreInfo);
			frame.renderFinishedSemaphore = this->mDevice.createSemaphore(semaphoreInfo);
			frame.inFlightFence = this->mDevice.createFence(fenceInfo);
		} catch(vk::SystemError e) {
			LOG_F(ERROR, "An error occured when creating sync objects: %s", e.what());
			throw std::runtime_error("Failed to create sync objects");
		}
	}
}

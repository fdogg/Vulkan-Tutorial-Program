#include "HelloTriangleApplication.hh"

void HelloTriangleApplication::CreateDescriptorSetLayout() {
	std::array<vk::DescriptorSetLayoutBinding, 2> bindings = {{
		// UBO layout binding
		{
			.binding = 0,
			.descriptorType = vk::DescriptorType::eUniformBuffer,
			.descriptorCount = 1,
			.stageFlags = vk::ShaderStageFlagBits::eAllGraphics,
			.pImmutableSamplers = nullptr
		},
		// Sampler layout binding
		{
			.binding = 1,
			.descriptorType = vk::DescriptorType::eCombinedImageSampler,
			.descriptorCount = 1,
			.stageFlags = vk::ShaderStageFlagBits::eFragment,
			.pImmutableSamplers = nullptr
		}
	}};

	vk::DescriptorSetLayoutCreateInfo layoutInfo {
		.bindingCount = static_cast<uint32_t>(bindings.size()),
		.pBindings = bindings.data()
	};

	this->mDescriptorSetLayout = this->mDevice.createDescriptorSetLayout(layoutInfo);
}

void HelloTriangleApplication::CreateDescriptorPool() {
	std::array<vk::DescriptorPoolSize, 2> poolSizes {{
		// UBO pool size
		{
			.type = vk::DescriptorType::eUniformBuffer,
			.descriptorCount = static_cast<uint32_t>(kMaxFramesInFlight)
		},
		// Image Sampler pool size
		{
			.type = vk::DescriptorType::eCombinedImageSampler,
			.descriptorCount = static_cast<uint32_t>(kMaxFramesInFlight)
		}
	}};

	vk::DescriptorPoolCreateInfo poolInfo {
		.maxSets = static_cast<uint32_t>(kMaxFramesInFlight),
		.poolSizeCount = static_cast<uint32_t>(poolSizes.size()),
		.pPoolSizes = poolSizes.data()
	};

	this->mDescriptorPool = this->mDevice.createDescriptorPool(poolInfo);
}

void HelloTriangleApplication::CreateDescriptorSets() {
	std::vector<vk::DescriptorSetLayout> layouts(kMaxFramesInFlight, this->mDescriptorSetLayout);
	vk::DescriptorSetAllocateInfo allocInfo {
		.descriptorPool = this->mDescriptorPool,
		.descriptorSetCount = static_cast<uint32_t>(kMaxFramesInFlight),
		.pSetLayouts = layouts.data()
	};

	std::vector<vk::DescriptorSet> descriptorSets = this->mDevice.allocateDescriptorSets(allocInfo);

	for(uint32_t i = 0; i < this->mFrames.size(); i++) {
		this->mFrames[i].descriptorSet = descriptorSets[i];
	}

	for(auto& frame : this->mFrames) {
		vk::DescriptorBufferInfo bufferInfo {
			.buffer = frame.uniformBuffer,
			.offset = 0,
			.range = sizeof(UniformBufferObject)
		};

		vk::DescriptorImageInfo imageInfo {
			.sampler = this->mTextureImageSampler,
			.imageView = this->mTextureImageView,
			.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal
		};

		std::array<vk::WriteDescriptorSet, 2> descriptorWrites {{
			// UBO dsecriptor set
			{
				.dstSet = frame.descriptorSet,
				.dstBinding = 0,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eUniformBuffer,
				.pImageInfo = nullptr,
				.pBufferInfo = &bufferInfo,
				.pTexelBufferView = nullptr
			},
			{
				.dstSet = frame.descriptorSet,
				.dstBinding = 1,
				.dstArrayElement = 0,
				.descriptorCount = 1,
				.descriptorType = vk::DescriptorType::eCombinedImageSampler,
				.pImageInfo = &imageInfo
			}
		}};

		this->mDevice.updateDescriptorSets(descriptorWrites, nullptr);
	}
}

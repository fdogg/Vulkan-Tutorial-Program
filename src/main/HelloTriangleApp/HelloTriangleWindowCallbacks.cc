#include "HelloTriangleApplication.hh"
#include "log/loguru.hh"
#include "imgui.h"

#include <GLFW/glfw3.h>
#include <chrono>

void HelloTriangleApplication::FramebufferResizeCallback(GLFWwindow *window, int width, int height) {

	// Stop unused variable warnings
	(void)width;
	(void)height;

	auto app = reinterpret_cast<HelloTriangleApplication*>(glfwGetWindowUserPointer(window));

	app->mFramebufferResized = true;
}

void HelloTriangleApplication::KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	auto app = reinterpret_cast<HelloTriangleApplication*>(glfwGetWindowUserPointer(window));

	// Goodbye unused variable warnings
	(void)scancode;
	(void)mods;

	// Quit key
	if(key == GLFW_KEY_ESCAPE) [[unlikely]] {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
		LOG_F(1, "%s", "Recieved escape key input");
	}

	// Toggling gui and flying
	if(key == GLFW_KEY_R && action == GLFW_PRESS) {
		ImGuiIO& io = ImGui::GetIO();
		if(app->mMouseEnabled) /* Fly mode */ {
			// Disable the cursor
			app->mMouseEnabled = false;
			io.ConfigFlags |= ImGuiConfigFlags_NoMouse;
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		} else /* ImGui mode */ {
			// Enable the cursor
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			app->mMouseEnabled = true;
			io.ConfigFlags &= ~ImGuiConfigFlags_NoMouse;
		}
	}
}

void HelloTriangleApplication::MouseCallback(GLFWwindow *window, double xpos, double ypos) {
	auto app = reinterpret_cast<HelloTriangleApplication*>(glfwGetWindowUserPointer(window));

	static float lasty = 0.0f;
	static float lastx = 0.0f;

	// Set y coord to be up
	ypos *= -1.0f;
	float dx = xpos - lastx;
	float dy = ypos - lasty;

#if defined(__linux__)
	if(dy < 0) dy *= 2.0f;
#endif

	constexpr float kScale = 4.5f;

	if(!app->mMouseEnabled) app->mCam.ProcessRotation(dx / kScale, dy / kScale);

	lastx = xpos;
	lasty = ypos;
}

#define KeyPressed(key) if(glfwGetKey(this->mWindow, (key)) == GLFW_PRESS)

void HelloTriangleApplication::PollKeyboardInput() {
	static auto lastTime = std::chrono::high_resolution_clock::now();
	auto currentTime = std::chrono::high_resolution_clock::now();
	float dt = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - lastTime).count();
	lastTime = currentTime;

	if(this->mMouseEnabled) return;

	// Speed in units/s
	const float speed = 1.0f;

	KeyPressed(GLFW_KEY_W) this->mCam.SetPositionWDelta({ 0.0f, 0.0f, speed }, dt);
	KeyPressed(GLFW_KEY_S) this->mCam.SetPositionWDelta({ 0.0f, 0.0f, -speed }, dt);
	KeyPressed(GLFW_KEY_A) this->mCam.SetPositionWDelta({ -speed, 0.0f, 0.0f }, dt);
	KeyPressed(GLFW_KEY_D) this->mCam.SetPositionWDelta({ speed, 0.0f, 0.0f }, dt);
	KeyPressed(GLFW_KEY_E) this->mCam.SetPositionWDelta({ 0.0f, speed, 0.0f }, dt);
	KeyPressed(GLFW_KEY_Q) this->mCam.SetPositionWDelta({ 0.0f, -speed, 0.0f }, dt);

}

#undef KeyPressed
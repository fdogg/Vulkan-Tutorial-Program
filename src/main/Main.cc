#include "HelloTriangleApp/HelloTriangleApplication.hh"

#include "log/loguru.hh"

#include <memory>

int main(int argc, char* argv[]) {
	if constexpr(!kDebug) {
		loguru::g_preamble_thread = false;
		loguru::g_preamble_file = false;
	}
	loguru::init(argc, argv);
	loguru::add_file("latest.log", loguru::Truncate, loguru::Verbosity_MAX);

	// Put the application's data on the heap
	std::unique_ptr<HelloTriangleApplication> app = std::make_unique<HelloTriangleApplication>();

	try {
		LOG_F(INFO, "Running app");
		app->Run();
	} catch(const std::exception& e) {
		LOG_F(ERROR, "An error occured while running the program: %s", e.what());
		return EXIT_FAILURE;
	} catch(...) {
		LOG_F(ERROR, "An unknown exception was thrown");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

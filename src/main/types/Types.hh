#pragma once

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

#include <glm/gtx/hash.hpp>

#include <vulkan/vulkan.hpp>

/**
 * @brief Struct that is used as a data input for the shaders
 */
struct Vertex {
	glm::vec3 position;
	glm::vec3 color;
	glm::vec2 texCoord;
	glm::vec3 normal;

	/**
	 * @brief Get the Binding Description for Vertex 
	 * 
	 * @return VkVertexInputBindingDescription The binding description for the Vertex struct
	 */
	static vk::VertexInputBindingDescription GetBindingDescription() noexcept;

	/**
	 * @brief Get the Attribute Descriptions of the Vertex members to be used in VBOs
	 * 
	 * @return std::array<VkVertexInputAttributeDescription, 2> The attribute descriptions for the struct members
	 */
	static std::array<vk::VertexInputAttributeDescription, 4> GetAttributeDescriptions() noexcept;

	/**
	 * @brief Comparison operator
	 * 
	 * @param other other Vertex to check against
	 * @return true/false if the other vertex is equal
	 */
	bool operator==(const Vertex& other) const;
};

/**
 * @brief Uniform Buffer Object
 */
struct UniformBufferObject {
	/**
	 * @brief Model matrix
	 */
	alignas(16) glm::mat4 model;

	/**
	 * @brief View (camera) matrix
	 */
	alignas(16) glm::mat4 view;

	/**
	 * @brief Projection matrix
	 */
	alignas(16) glm::mat4 proj;

	/**
	 * @brief Material specular
	 */
	alignas(16) glm::vec3 mspec;

	/**
	 * @brief Material diffuse
	 */
	alignas(16) glm::vec3 mdiff;

	/**
	 * @brief Ambient
	 */
	alignas(4) float amb;

	/**
	 * @brief Shininess
	 */
	alignas(4) float mshin;

	/**
	 * @brief Light specular
	 */
	alignas(16) glm::vec3 lspec;

	/**
	 * @brief Light diffuse
	 */
	alignas(16) glm::vec3 ldiff;

	/**
	 * @brief Light position
	 */
	alignas(16) glm::vec3 lpos;

	/**
	 * @brief Camera position
	 */
	alignas(16) glm::vec3 campos;
};

namespace std {

template<>
struct hash<Vertex> {
	size_t operator()(Vertex const& vert) const {
		return (
			(hash<glm::vec3>()(vert.position) ^
			(hash<glm::vec3>()(vert.color) << 1)) >> 1
		) ^ (hash<glm::vec2>()(vert.texCoord) << 1);
	}
};

};
#include "types/Types.hh"

vk::VertexInputBindingDescription Vertex::GetBindingDescription() noexcept {
	vk::VertexInputBindingDescription bindDescription {
		.binding = 0,
		.stride = sizeof(Vertex),
		.inputRate = vk::VertexInputRate::eVertex
	};

	return bindDescription;
}

std::array<vk::VertexInputAttributeDescription, 4> Vertex::GetAttributeDescriptions() noexcept {
	std::array<vk::VertexInputAttributeDescription, 4> attribDescriptions { };

	// Position attribute
	attribDescriptions[0] = {
		.location = 0,
		.binding = 0,
		.format = vk::Format::eR32G32B32Sfloat,
		.offset = offsetof(Vertex, position)
	};

	// Color attribute
	attribDescriptions[1] = {
		.location = 1,
		.binding = 0,
		.format = vk::Format::eR32G32B32Sfloat,
		.offset = offsetof(Vertex, color)
	};

	// Tex coord attribute
	attribDescriptions[2] = {
		.location = 2,
		.binding = 0,
		.format = vk::Format::eR32G32Sfloat,
		.offset = offsetof(Vertex, texCoord)
	};

	// Normal attribute
	attribDescriptions[3] = {
		.location = 3,
		.binding = 0,
		.format = vk::Format::eR32G32B32Sfloat,
		.offset = offsetof(Vertex, normal)
	};

	return attribDescriptions;
}

bool Vertex::operator==(const Vertex& other) const {
	return this->position == other.position && this->color == other.color && this->texCoord == other.texCoord;
}
#include "Camera.hh"

#include <glm/ext/matrix_transform.hpp>
#include <glm/trigonometric.hpp>
#include <glm/geometric.hpp>

namespace utils {

Camera::Camera(glm::vec3 position, glm::vec3 up, float yaw, float pitch):
	mPos(position),
	mUp(),
	mFront(0.0f, 0.0f, -1.0f),
	mWorldUp(up),
	mRight(),
	mPitch(pitch),
	mYaw(yaw) {

	this->Update();
}

void Camera::Update() {
	float yawRad = glm::radians(this->mYaw);
	float pitchRad = glm::radians(this->mPitch);
	float cosPitch = std::cos(pitchRad);

	glm::vec3 front;
	front.x = cos(yawRad) * cosPitch;
	front.y = sin(pitchRad);
	front.z = sin(yawRad) * cosPitch;
	this->mFront = glm::normalize(front);

	this->mRight = glm::normalize(glm::cross(this->mFront, this->mWorldUp));
	this->mUp = glm::normalize(glm::cross(this->mRight, this->mFront));
}

glm::mat4 Camera::GetCameraMatrix() const noexcept {
	return glm::lookAt(this->mPos, this->mPos + this->mFront, this->mUp);
}

void Camera::SetPosition(glm::vec3 pos) {
	if(this->mPos == pos) return;
	this->mPos = this->mUp * pos.y;
	this->mPos += this->mFront * pos.z;
	this->mPos += this->mRight * pos.x;
}

void Camera::SetPositionWDelta(glm::vec3 vel, float dt) {
	this->mPos += vel.x * this->mRight * dt;
	this->mPos += vel.y * this->mUp * dt;
	this->mPos += vel.z * this->mFront * dt;
}

float Camera::GetPitch() const noexcept {
	return this->mPitch;
}

float Camera::GetYaw() const noexcept {
	return this->mYaw;
}

void Camera::ProcessRotation(float x, float y) {
	this->mPitch += y;
	this->mYaw += x;

	this->CheckRotation();
	this->Update();
}

void Camera::CheckRotation() noexcept {
	if(this->mPitch > 89.0f) [[unlikely]]
		this->mPitch = 89.0f;

	if(this->mPitch < -89.0f) [[unlikely]]
		this->mPitch = -89.0f;
}

void Camera::SetPitchYaw(float pitch, float yaw) {
	// Return if same values
	if(this->mPitch == pitch && this->mYaw == yaw) return;

	this->mPitch = pitch;
	this->mYaw = yaw;

	this->CheckRotation();
	this->Update();
}

const glm::vec3 Camera::GetPosition() const noexcept {
	return this->mPos;
}

} // namespace utils
#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

namespace utils {

/**
 * @brief Encapsulates a view matrix as a first person camera
 *
 * This class is used for a fly around first person camera that should
 * probably be used for deubgging purposes or something similar.
 */
class Camera {
private:
	glm::vec3 mPos;
	glm::vec3 mUp;
	glm::vec3 mFront;
	glm::vec3 mWorldUp;
	glm::vec3 mRight;

	float mPitch;
	float mYaw;

public:
	/**
	 * @brief Constructs a Camera
	 * 
	 * @param position Initial position
	 * @param up Up vector
	 * @param yaw Initial yaw (degrees)
	 * @param pitch Initial pitch (degrees)
	 */
	Camera(
		glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
		float yaw = -90.0f, float pitch = 0.0f
	);

private:
	/**
	 * @brief Updates the vectors each render loop
	 */
	void Update();

	/**
	 * @brief Clamps rotation if it is out of bounds
	 */
	void CheckRotation() noexcept;

public:
	/**
	 * @brief Updates the camera vectors and gets the Camera matrix (View matrix)
	 * 
	 * @return glm::mat4 The view matrix
	 */
	glm::mat4 GetCameraMatrix() const noexcept;
	
	/**
	 * @brief Sets the camera position
	 * 
	 * @param pos Camera position
	 */
	void SetPosition(glm::vec3 pos);

	/**
	 * @brief Sets the camera position over time
	 * 
	 * @param dir Velocity
	 * @param dt Time
	 */
	void SetPositionWDelta(glm::vec3 vel, float dt);

	/**
	 * @brief Gets the Pitch
	 * 
	 * @return float The camera pitch
	 */
	float GetPitch() const noexcept;

	/**
	 * @brief Gets the Yaw
	 * 
	 * @return float The camera yaw
	 */
	float GetYaw() const noexcept;

	/**
	 * @brief Integrates the mouse offsets onto the current pitch & yaw
	 * 
	 * @param x Mouse deltaX (yaw)
	 * @param y Mouse deltaY (pitch)
	 */
	void ProcessRotation(float x, float y);

	/**
	 * @brief Sets the pitch and yaw
	 * 
	 * @param pitch The pitch
	 * @param yaw The yaw
	 */
	void SetPitchYaw(float pitch, float yaw);

	/**
	 * @brief Gets the position of the camera
	 * 
	 * @return const glm::vec3 The camera's position in world space
	 */
	const glm::vec3 GetPosition() const noexcept;
};

} // namespace utils
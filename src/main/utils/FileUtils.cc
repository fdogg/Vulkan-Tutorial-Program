#include "FileUtils.hh"

#include <fstream>

namespace utils {

const std::vector<char> ReadFile(const std::string_view filename) {
	std::ifstream file(filename.data(), std::ios::ate | std::ios::binary);

	if(!file.is_open()) [[unlikely]] {
		throw std::runtime_error("Failed to open file");
	}

	size_t fileSize = file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	return buffer;
}

}

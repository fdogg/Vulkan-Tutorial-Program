#pragma once

#include <vector>
#include <string_view>

namespace utils {

/**
 * @brief Puts the contents of a file into a vector
 * 
 * @param filename Name of the file
 * @return std::vector<char> The contents of the file
 */
const std::vector<char> ReadFile(const std::string_view filename);

}

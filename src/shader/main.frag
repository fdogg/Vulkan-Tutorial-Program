#version 460

layout(std140, set = 0, binding = 0) uniform UniformBufferObject {
	mat4 model;
	mat4 view;
	mat4 proj;
	vec3 matSpec;
	vec3 matDiff;
	float ambient;
	float matShininess;
	vec3 lightSpec;
	vec3 lightDiff;
	vec3 lightpos;
	vec3 campos;
} ubo;

layout(set = 0, binding = 1) uniform sampler2D uTexSampler;

layout(location = 0) in vec3 iFragColor;
layout(location = 1) in vec2 iTexCoord;
layout(location = 2) in vec3 iNormal;
layout(location = 3) in vec3 iToLight;
layout(location = 4) in vec3 iToEye;

layout(location = 0) out vec4 oColor;

vec4 ambient() {
	return ubo.ambient * texture(uTexSampler, iTexCoord);
}

vec4 diffuse() {
	return vec4(ubo.matDiff * ubo.lightDiff * clamp(dot(iNormal, iToLight), 0.0, 1.0), 1.0);
}

vec4 specular() {
	if(dot(iNormal, iToLight) > 0) {
		return vec4(ubo.matSpec * ubo.lightSpec * pow(dot(iNormal, normalize(iToLight + iToEye)), ubo.matShininess), 1.0);
	} else {
		return vec4(0.0, 0.0, 0.0, 1.0);
	}
}

void main() {
	oColor = ambient() + diffuse() + specular();
}

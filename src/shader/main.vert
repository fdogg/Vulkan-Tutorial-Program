#version 460

layout(std140, set = 0, binding = 0) uniform UniformBufferObject {
	mat4 model;
	mat4 view;
	mat4 proj;
	vec3 matSpec;
	vec3 matDiff;
	float ambient;
	float matShininess;
	vec3 lightSpec;
	vec3 lightDiff;
	vec3 lightpos;
	vec3 campos;
} ubo;

// Inputs
layout(location = 0) in vec3 iPosition;
layout(location = 1) in vec3 iColor;
layout(location = 2) in vec2 iTexCoord;
layout(location = 3) in vec3 iNormal;

// Outputs
layout(location = 0) out vec3 oColor;
layout(location = 1) out vec2 oTexCoord;
layout(location = 2) out vec3 oNormal;
layout(location = 3) out vec3 oToLight;
layout(location = 4) out vec3 oToEye;

out gl_PerVertex {
	vec4 gl_Position;
};

void main() {
	// Apply transformations
	vec4 worldPos = ubo.model * vec4(iPosition, 1.0);
	gl_Position = ubo.proj * ubo.view * worldPos;
	oColor = iPosition;
	oTexCoord = iTexCoord;
	oNormal = normalize(worldPos.xyz + iNormal);
	oToLight = normalize(ubo.lightpos - worldPos.xyz);
	oToEye = normalize(ubo.campos - worldPos.xyz);
}
